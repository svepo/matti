/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement; 
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.Timer;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.h2.value.CaseInsensitiveMap;



public class Database {
	
	static String dbPath;
	Connection conn = null;
	
	
	
	public Connection connectToDatabase () {
		 loadParams();
		 	
	     try {
					Class.forName("org.h2.Driver");		
	     		}
	    catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	     try {       
	            
	           String dbPrefix = "jdbc:h2:";
	           String pathToDb = checkPath(dbPrefix);
	           //System.out.println("connect: pathToDb: "+pathToDb+" dbPath: "+dbPath);
	           conn = DriverManager.getConnection(pathToDb, "", "");
			
		 	} 
	     catch (SQLException e) {
	            e.printStackTrace();
		 		}
		return conn;
	 }
	 
	private String checkPath(String dbPrefix) {
		// checks whether the path to the db has the right prefix e.g. "jdbc:h2"
		String pathToDb;
		Boolean prefix = StringUtils.startsWith(dbPath, dbPrefix);
        if (prefix) {
        	pathToDb = dbPath;
		} else {
			pathToDb = dbPrefix+dbPath;
		}
		return pathToDb;
	}

	public void writeMemberToDB (String table, String name, String vorname, String phone, String mail, int maxArtikel, String tableName) throws SQLException {
		 //Schreibt die Mitgliedsdaten in die Tabelle und erstellt Sie falls sie noch nicht existiert
		 Statement stmt = null;
		 
		 try {
				stmt = connectToDatabase().createStatement();
				 String createQ = "CREATE TABLE IF NOT EXISTS "
						+ table
		                + "(NAME VARCHAR(255), VORNAME VARCHAR(255), PHONE VARCHAR(255), MAIL VARCHAR(255), MAX_ARTIKEL TINYINT, TABLENAME VARCHAR(255))";
		         stmt.executeUpdate(createQ);
		         conn.close();
			} catch (SQLException e) {
				System.err.println("Tabelle "+table+" konnte nicht angelegt werden!");
				e.printStackTrace();
			}
		 try {
			 stmt = connectToDatabase().createStatement();
			 String insertQ = "INSERT INTO " + table
	                + " VALUES('"+name+"','"+vorname+"','"+phone+"','"+mail+"','"+maxArtikel+"','"+tableName+"')";
			 //System.out.println(insertQ);
	         stmt.executeUpdate(insertQ);
	         //DbWrite(tableName,0,"","",true);
	         conn.close();
		 } catch (SQLException e) {
			 System.err.println("Fehler beim Schreiben in Tabelle "+table+" !");
			 e.printStackTrace();
		 }
		 
   }
	
	public void updateMemberData (String table, String name, String vorname, String phone, String mail, int maxArtikel) throws SQLException {
		 //Schreibt die Mitgliedsdaten in die Tabelle und erstellt Sie falls sie noch nicht existiert
		 Statement stmt = null;
		 
		
		 try {
			 stmt = connectToDatabase().createStatement();
			String insertQ = "UPDATE " + table + " SET PHONE = '"+ phone + "' ,MAIL = '"+ mail + "' ,MAX_ARTIKEL = '"+ maxArtikel+"' WHERE NAME = '"+name+ "' AND VORNAME = '"+vorname+"'";
		
	         stmt.executeUpdate(insertQ);
	         //DbWrite(tableName,0,"","",true);
	         conn.close();
		 } catch (SQLException e) {
			 System.err.println("Fehler beim Schreiben in Tabelle "+table+" !");
			 e.printStackTrace();
		 }
		 
  }
	
	public void deleteMember(String table, String name, String vorname) {
		//Löscht den Eintrag in der Mitgliedstabelle und die dazugehörige Artikeltabelle
		Statement stmt = null;
		try {
			 stmt = connectToDatabase().createStatement();
			 String dropTable = "DROP TABLE IF EXISTS " + table;
	         stmt.executeUpdate(dropTable);
	         stmt.executeUpdate("DELETE FROM MITGLIEDER WHERE NAME = '"+name+"' AND VORNAME = '"+vorname+"'");
	         conn.close();
		}
		catch (SQLException e) {
			System.err.println("Tabelle "+table+" konnte nicht gelöscht werden!");
			e.printStackTrace();
		}
			 
	}
	
	public void writeToTable (String table, int artikelNr, String leihArt, String leihDate, Boolean isReturned) throws SQLException {
		 //Schreibt die Daten in die Tabelle und erstellt Sie falls sie noch nicht existiert
		
		Statement stmt = null;
		String returnDate = " "; 
		 try {
				stmt = connectToDatabase().createStatement();
				 String createQ = "CREATE TABLE IF NOT EXISTS "
						+ table
		                + "(ARTIKEL_NR INT, ARTIKEL_ART VARCHAR(255), DATUM VARCHAR(10), MAXART_ID INT, ISRETURNED BOOLEAN, RETURNDATE VARCHAR(10))";
		         stmt.executeUpdate(createQ);
		         stmt.close();
		         conn.close();
			} catch (SQLException e) {
				System.err.println("Tabelle "+table+" konnte nicht angelegt werden!");
				e.printStackTrace();
			}
		 try {
			 stmt = connectToDatabase().createStatement();
			 int count = countRows(table);
			 String insertQ = "INSERT INTO " + table
	                + " VALUES('"+artikelNr+"','"+leihArt+"','"+leihDate+"','"+count+"','"+isReturned+"','"+returnDate+"')";
			 //System.out.println(insertQ);
	         stmt.executeUpdate(insertQ);
	         conn.close();
		 } catch (SQLException e) {
			 System.err.println("Fehler beim Schreiben in Tabelle "+table+" !");
			 e.printStackTrace();
		 }
		 
    }
	
	//Leihartikel Tabelle
	public void writeToArticleTable (String table, String artikel, int stock, int artNumber, int stockAvailable) throws SQLException {
		 //Schreibt die Daten in die Tabelle und erstellt Sie falls sie noch nicht existiert
		 Statement stmt = null;
		 
		 try {
				stmt = connectToDatabase().createStatement();
				 String createQ = "CREATE TABLE IF NOT EXISTS "
						+ table
		                + "(ID INT AUTO_INCREMENT , ARTIKEL VARCHAR(255), ARTIKEL_STOCK INT, ARTIKEL_NR INT, ARTIKEL_STOCK_AVAILABLE INT)";
		         stmt.executeUpdate(createQ);
		         //Fuege die erste leere Zeile ein
		         String insertQ = "INSERT INTO " + table
			                + " VALUES('','0','100','0')";
		         stmt.executeUpdate(createQ);
		         stmt.close();
		         conn.close();
			} catch (SQLException e) {
				System.err.println("Tabelle "+table+" konnte nicht angelegt werden!");
				e.printStackTrace();
			}
		 
		try {
			 stmt = connectToDatabase().createStatement();
	
			 String insertQ = "INSERT INTO " + table
		                + " VALUES('"+artikel+"','"+stock+"','"+artNumber+"','"+stockAvailable+"')";
			 //System.out.println(insertQ);
	         stmt.executeUpdate(insertQ);
	         stmt.close();
	         conn.close();
	        // System.out.println("Artikel gespeichert!\n");
		 } catch (SQLException ex) {
			 System.err.println("Fehler beim Schreiben in Tabelle "+table+" !");
			 ex.printStackTrace();
		 }
	
   }
	
	public void updateArticleTable (String table, int stock, int artNumber) throws SQLException {
		//Übergibt den neuen Bestand
		Statement stmt = null;
		try {
			 stmt = connectToDatabase().createStatement();
			 int count = countRows(table);
			 String insertQ = "UPDATE " + table
	                + " SET ARTIKEL_STOCK = "+ stock + " WHERE ARTIKEL_NR = "+artNumber;
			 //TODO Where Klausel mit ID nicht mit artikel und beschreibung!!!
			 //System.out.println(insertQ);
	         stmt.executeUpdate(insertQ);
	         stmt.close();
	         conn.close();
	         //System.out.println("Bestand aktualisiert!");
		 } catch (SQLException e) {
			 System.err.println("Fehler beim Schreiben in Tabelle "+table+" !");
			 e.printStackTrace();
		 }
	}
	
	public void updateArticleTable (String table, String article, int stock, int artNumber) throws SQLException {
		//Übergibt den neuen Bestand
		Statement stmt = null;
		try {
			 stmt = connectToDatabase().createStatement();
			 int count = countRows(table);
			
			 String insertQ = "UPDATE " + table
		                + " SET ARTIKEL_STOCK = "+ stock + " WHERE ARTIKEL_NR = "+artNumber;
			 
			 //System.out.println(insertQ);
	         stmt.executeUpdate(insertQ);
	         insertQ = "UPDATE " + table
		                + " SET ARTIKEL = '"+ article + "' WHERE ARTIKEL_NR = "+artNumber;
	         stmt.executeUpdate(insertQ);
	         conn.close();
	         JOptionPane.showConfirmDialog(null, "Artikel aktualisiert",
	 		        "Matti", JOptionPane.OK_CANCEL_OPTION);
	         //System.out.println("Bestand aktualisiert!");
		 } catch (SQLException e) {
			 System.err.println("Fehler beim Schreiben in Tabelle "+table+" !");
			 e.printStackTrace();
		 }
	}
	
	public ResultSet readFromDatabase(String table) {
		//liest die Daten aus der übergebenden TABLE und speichert sie in einem ResultSet
		ResultSet selectRS = null;
		Statement stmt = null;
		try {
			 stmt = connectToDatabase().createStatement();
			 selectRS = stmt.executeQuery("SELECT * FROM " + table+ " ORDER BY ISRETURNED, MAXART_ID");
			 //stmt.close();
		} catch (SQLException e) {
			 System.err.println("...kein Eintrag vorhanden!");
			 
		}
		
		return selectRS;
		
	}
	
	public String[] readFromTableAndStoreIntoArray(String table, int id) {
		//liest Artikel und Datum aus der übergebenden TABLE und speichert sie in einem array
		ResultSet selectRS = null;
		int rsLength = countNotReturnedRows(table);
		Statement stmt = null;
		try {
			 stmt = connectToDatabase().createStatement();
			 selectRS = stmt.executeQuery("SELECT ARTIKEL_NR, DATUM, MAXART_ID FROM " + table+ " WHERE ISRETURNED = false ORDER BY MAXART_ID ASC");
			 //stmt.close();
		} catch (SQLException e) {
			 System.err.println("... kein Eintrag vorhanden!");
			 
		}
		String[] selectedStr = new String[rsLength];
		switch (id) {
		case 0:
			try {
				int i = 0;
				while (selectRS.next()) {
					selectedStr[i]=selectRS.getString(1);
					i++;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			break;
		case 1:
			try {
				int i = 0;
				while (selectRS.next()) {
					selectedStr[i]=selectRS.getString(2);
					i++;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		case 2:
			try {
				int i = 0;
				while (selectRS.next()) {
					selectedStr[i]=String.valueOf(selectRS.getString(3));
					i++;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		default:
			break;
		}
		
		//System.out.println(selectedStr);
		return selectedStr;
		
	}
	
	public String[] readRow(String table, int id) {
		//liest die Daten aus der übergebenden TABLE und speichert sie in einem ResultSet
		String[] selectedStr = new String[4];
		ResultSet selectedRS = null;
		Statement stmt = null;
		
		try {
			 stmt = connectToDatabase().createStatement();
			 //String query = ("SELECT ARTIKEL, ARTIKEL_ART, DATUM FROM " +table+" WHERE MAXART_ID = "+id);
			 selectedRS = stmt.executeQuery("SELECT * FROM "+table+" WHERE MAXART_ID = '"+id+"'");
			
		} catch (SQLException e) {
			 System.err.println("...kein Eintrag vorhanden!");
			 
		}
		
		try {
			int i = 0;
			while (selectedRS.next()) {
				selectedStr[i]=(selectedRS.getString(i+1));
				selectedStr[i+1]=(selectedRS.getString(i+2));
				selectedStr[i+2]=(selectedRS.getString(i+3));
				//selectedStr[i+3]=(selectedRS.getString(i+4));
				i++;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return selectedStr;
		
	}
	
	public String[] readArticleRow(String table, int artNummer) {
		//liest die Daten aus der übergebenden TABLE und speichert sie in einem ResultSet
		String[] selectedStr = new String[4];
		ResultSet selectedRS = null;
		Statement stmt = null;
		
		try {
			 stmt = connectToDatabase().createStatement();
			 
			 selectedRS = stmt.executeQuery("SELECT ARTIKEL, ARTIKEL_STOCK, ARTIKEL_STOCK_AVAILABLE FROM "+table+" WHERE ARTIKEL_NR = '"+artNummer+"'");
			 //System.out.println(query);
			 //stmt.close();
			 
		} catch (SQLException e) {
			 System.err.println("...kein Eintrag vorhanden!");
			 
		}
		
		try {
			int i = 1;
			while (selectedRS.next()) {
				selectedStr[i]=(selectedRS.getString(i));
				//selectedStr[i+1]=selectedRS.getString(i+1);
				selectedStr[i+1]= String.valueOf(selectedRS.getInt(i+1));
				selectedStr[i+2]= String.valueOf(selectedRS.getInt(i+2));
				i++;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return selectedStr;
		
	}
	
	public String[] getTables(String tableMitglieder, int key) {
		//liest alle Mitgliedstabellen aus oder Tabellennamen und übergibt sie
		Statement stmt = null;
		
		String[] member = new String[countRows(tableMitglieder)];
		
		
		switch (key) {
		case 1:
			try {
				stmt = connectToDatabase().createStatement();
				
				ResultSet queryRS = stmt.executeQuery("SELECT NAME,VORNAME FROM " + tableMitglieder);
				
				int i = 1;
				while (queryRS.next()) {
					member[i-1]= queryRS.getString(1)+", "+queryRS.getString(2);
					
					i++;
	
				}
		//stmt.close();
		conn.close();		
		 }
		 catch (SQLException e) {
			 System.err.println("Fehler beim Abfragen der Tabellen!");
		 }
			
		break;
		case 2:
			try {
				stmt = connectToDatabase().createStatement();
				
				//ResultSet queryRS = stmt.executeQuery("SELECT NAME,VORNAME FROM " + tableMitglieder);
				ResultSet tableNameRS = stmt.executeQuery("SELECT TABLENAME FROM " + tableMitglieder);
				int i = 1;
				while (tableNameRS.next()) {
					//member[i-1]= queryRS.getString(1)+", "+queryRS.getString(2);
					member[i-1] = tableNameRS.getString(1);
					i++;
	
				}
		//stmt.close();
		conn.close();		
		 }
		 catch (SQLException e) {
			 System.err.println("Fehler beim Abfragen der Tabellen!");
		 }
		break;

		default:
			break;
		}
		 
		return member;  
	}
	
	public int getArticleTables(String table) {
		//liest alle Artikel aus und gibt die letzte Artikelnummer zurück
		Statement stmt = null;
		Helper helpString = new Helper(); 
		int lastArtNum = -1;
		
		 try {
				stmt = connectToDatabase().createStatement();
				
				ResultSet queryRS = stmt.executeQuery("SELECT * FROM " + table +" ORDER BY ARTIKEL_NR");
				
				//ermittle die letzte Artikelnummer
				queryRS = stmt.executeQuery("SELECT MAX(ARTIKEL_NR) AS LASTARTNR FROM "+ table);
				while (queryRS.next()) {
						lastArtNum = queryRS.getInt(1);
					
				}
				
		//stmt.close();		
		conn.close();
		 }
		 catch (SQLException e) {
			 System.err.println("Fehler beim Abfragen der Tabellen!**");
		 }
	return lastArtNum;	  
	}
	
	public String[] getArticleArray(String table, int key) {
		//liest alle Artikel aus und übergibt Sie in einem Array
		Statement stmt = null;
		Helper helpString = new Helper(); 
		int lastArtNum = -1;
		String[] artikel = new String[countRows(table)];
		//System.out.println(artikel.length);
		 try {
				stmt = connectToDatabase().createStatement();
				
				ResultSet queryRS = stmt.executeQuery("SELECT * FROM " + table +" ORDER BY ARTIKEL_NR");
				
				int i = 1;
				
				switch (key) {
				
				case 0:
					while (queryRS.next()) {
						artikel[i-1] = queryRS.getString(3)+" - "+queryRS.getString(1);
						i++;
					}
				case 1:
					while (queryRS.next()) {
						artikel[i-1] = queryRS.getString(3);
						i++;
					}
				case 2:
					while (queryRS.next()) {
						artikel[i-1] = queryRS.getString(2);
						i++;
					}
				case 3:
					while (queryRS.next()) {
						artikel[i-1] = queryRS.getString(1);
						i++;
					}	
				default:
					while (queryRS.next()) {
						artikel[i-1] = "< "+queryRS.getString(3)+" >\t"+queryRS.getString(1)+"\t aktueller Bestand: "+queryRS.getString(4);
		                i++;
					}
					break;
				}
				
				
		//stmt.close();		
		conn.close();
		
		 }
		 catch (SQLException e) {
			 System.err.println("Fehler beim Abfragen der Tabellen!DbGetArtikelArray");
		 }
	return artikel;
	}
	
	public String[] returnTable(int selection, String tableMitglieder) {
		//ermittelt aus dem gewählten Mitglied die TABLENAME als Rückgabewert 
		Statement stmt = null;
		Helper helpString = new Helper(); 
		HashMap tableMap = new HashMap();
		HashMap nameMap = new HashMap();
		HashMap vornameMap = new HashMap();
		HashMap phoneMap = new HashMap();
		HashMap mailMap = new HashMap();
		HashMap maxartMap = new HashMap();
		String[] returnString = new String[6];
		
		 try {
				stmt = connectToDatabase().createStatement();
				ResultSet tableRS = stmt.executeQuery("SELECT NAME,VORNAME,PHONE, MAIL, MAX_ARTIKEL,TABLENAME FROM " + tableMitglieder);
				
				int i = 1;
				while (tableRS.next()) {
					tableMap.put(i, new String (tableRS.getString(6)) );
					nameMap.put(i, new String (tableRS.getString(1)) );
					vornameMap.put(i, new String (tableRS.getString(2)) );
					phoneMap.put(i, new String (tableRS.getString(3)) );
					mailMap.put(i, new String (tableRS.getString(4)) );
					Integer maxInt = new Integer (tableRS.getInt(5));
					String maxIntStr = maxInt.toString();
					maxartMap.put(i, maxIntStr );
	               
					i++;
				}
		conn.close();		
		 }
		 catch (SQLException e) {
			 System.err.println("Fehler beim Abfragen der Tabellen!");
		 }
		
		//Das ist das gewählte Mitglied
		String table = (String) tableMap.get(selection);
		String selectedName = (String) nameMap.get(selection);
		String selectedVorname = (String) vornameMap.get(selection);
		String selectedMaxArt = (String) maxartMap.get(selection);
		String selectedPhone = (String) phoneMap.get(selection);
		String selectedMail = (String) mailMap.get(selection);
		
		//Packe das in ein Array
		returnString[0] = selectedName;
		returnString[1] = selectedVorname;
		returnString[2] = table;
		returnString[3] = selectedMaxArt;
		returnString[4] = selectedPhone;
		returnString[5] = selectedMail;
		//System.out.println(returnString[0]+","+returnString[1]+","+returnString[2]);
		return returnString;
	}
	
	public void deleteRecord (String table, int id) {
		//löscht eine Zeile in Tabelle
		Statement stmt = null;
		try {
			 stmt = connectToDatabase().createStatement();
			 stmt.executeUpdate("DELETE FROM " + table+" WHERE MAXART_ID = '"+id+"'");
			 //System.out.println("Datensatz gelöscht!");
		} catch (SQLException e) {
			 System.err.println("Fehler beim Löschen des Datensatzes!");
			 
		}
	}
	
	public void isReturnedRecord (String table, int id, Boolean returned, String returnDate) {
		//ändert eine Zeile in Tabelle zu ISRETURNED: true und fügt das Rückgabedatum ein
		Statement stmt = null;
		try {
			 String updateStr = "UPDATE " + table +" SET ISRETURNED = " + returned +" ,RETURNDATE = '"+ returnDate +"' WHERE MAXART_ID = '"+id+"'";
			 //System.out.println(updateStr);
			 stmt = connectToDatabase().createStatement();
			 stmt.executeUpdate(updateStr);
			 
			 //System.out.println("Datensatz gelöscht!");
		} catch (SQLException e) {
			 System.err.println("Fehler beim Löschen des Datensatzes!");
			 
		}
	}
	
	public int countRows (String table) {
		//Ermittelt die Anzahl der Rows
		Statement stmt = null;
		int count = -1;
		
		try {
			stmt = connectToDatabase().createStatement();
			String sql = "SELECT COUNT (*) FROM "+table;
			ResultSet rs = stmt.executeQuery(sql);
			//
			if (rs.next())
			{
			  count = rs.getInt(1);
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return count;
		
	}
	
	public int countNotReturnedRows (String table) {
		//Ermittelt die Anzahl der Rows
		Statement stmt = null;
		int count = -1;
		
		try {
			stmt = connectToDatabase().createStatement();
			String sql = "SELECT COUNT (*) FROM "+table+" WHERE ISRETURNED = false";
			ResultSet rs = stmt.executeQuery(sql);
			//
			if (rs.next())
			{
			  count = rs.getInt(1);
			}
		} catch (SQLException e) {
			count = 0;
		}
		return count;
		
	}
	
	public void getArticle(String table, int artikelNr) {
		//liest zur Artikelnummer den Artikel aus und übergibt ihn mit Setter
		Artikel selectedArticle = new Artikel();
		String[] selectedArt = new String[5];
		selectedArt = readArticleRow(table, artikelNr);
		selectedArticle.setArtikel(selectedArt[1]);
		selectedArticle.setArtikelNummer(artikelNr);
		selectedArticle.setArtikelStock(Integer.parseInt(selectedArt[2]));
		selectedArticle.setStockAvailable(Integer.parseInt(selectedArt[4]));
	}
	
	public void createAllTables (String tableMember, String tableArticle) {
		//beim ersten Start von Matti werden alle Tables angelegt
		Statement stmt = null;
		 
		 try {
				stmt = connectToDatabase().createStatement();
				 String createQ = "CREATE TABLE IF NOT EXISTS "
						+ tableMember
		                + "(NAME VARCHAR(255), VORNAME VARCHAR(255), PHONE VARCHAR(255), MAIL VARCHAR(255), MAX_ARTIKEL TINYINT, TABLENAME VARCHAR(255))";
		         stmt.executeUpdate(createQ);
		         conn.close();
		         
		        
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Tabelle "+tableMember+" konnte nicht angelegt werden!",
				        "Matti",  JOptionPane.ERROR_MESSAGE);
				System.err.println("Tabelle "+tableMember+" konnte nicht angelegt werden!");
				e.printStackTrace();
			}
		 try {
				stmt = connectToDatabase().createStatement();
				 String createQ = "CREATE TABLE IF NOT EXISTS "
						+ tableArticle
		                + "(ARTIKEL VARCHAR(255), ARTIKEL_STOCK INT, ARTIKEL_NR INT, ARTIKEL_STOCK_AVAILABLE INT)";
		         stmt.executeUpdate(createQ);
		         //Fuege die erste leere Zeile ein
		         String insertQ = "INSERT INTO " + tableArticle
			                + " VALUES('','0','100','0')";
		         stmt.executeUpdate(createQ);
		         stmt.close();
		         conn.close();
		         
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Tabelle "+tableArticle+" konnte nicht angelegt werden!",
				        "Matti",  JOptionPane.ERROR_MESSAGE);
				System.err.println("Tabelle "+tableArticle+" konnte nicht angelegt werden!");
				e.printStackTrace();
			}
		 
		
	}
	
	public static void loadParams() {
	    Properties props = new Properties();
	    InputStream is = null;
	 
	    // Lade Datei
	    try {
	        File f = new File("matti.ini");
	        is = new FileInputStream( f );
	    }
	    catch ( Exception e ) { is = null; }
	 
	    try {
	      
	        props.loadFromXML(is);
	    }
	    catch ( Exception e ) { }
	 
	    String defaultPath = "jdbc:h2:"+System.getProperty("user.dir")+System.getProperty("file.separator")+"Database"+System.getProperty("file.separator")+"matti_db";
	    
	    dbPath = props.getProperty("PATH_TO_DB", defaultPath);
	  
	    
	}
}