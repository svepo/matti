/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

public class Artikel {
	
	String artikel;
	int nummer;
	int stock = 0;
	int stockAvailable = 0;
	Boolean isNewArticle = false;
	
	public void artikel(String a1, String a2, String a3, String a4, String a5) {
		
	}
	
	public String getArtikel() {
		return artikel;
	}

	public void setArtikel(String artikel) {
		this.artikel = artikel;
	}
	
	public int getArtikelNummer() {
		return nummer;
	}

	public void setArtikelNummer(int nummer) {
		this.nummer = nummer;
	}
	
	public int getArtikelStock() {
		return stock;
	}

	public void setArtikelStock(int stock) {
		this.stock = stock;
	}

	public int getStockAvailable() {
		return stockAvailable;
	}

	public void setStockAvailable(int stockAvailable) {
		this.stockAvailable = stockAvailable;
	}

	public Boolean getIsNewArticle() {
		return isNewArticle;
	}

	public void setIsNewArticle(Boolean isNewArticle) {
		this.isNewArticle = isNewArticle;
		System.out.println(isNewArticle);
	}


}
