/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.swing.JOptionPane;

public class SendMail
{

   public void Send(String subject, String messageText, String to, String from, String host, final String user, final String password, String port, boolean tlsenable, boolean smtpAuth )
   {    
      

      // Get system properties
      final Properties properties = System.getProperties();
      
     

      // Setup mail server
      properties.setProperty("mail.smtp.host", host);
      properties.setProperty("mail.smtp.user", user);
      properties.setProperty("mail.smtp.starttls.enable", String.valueOf(tlsenable));
      properties.setProperty("mail.smtp.auth", String.valueOf(smtpAuth));
      properties.setProperty("mail.smtp.port",port);
      
    

      // Get the default Session object.
      Session session = Session.getDefaultInstance(properties, 
    		new javax.mail.Authenticator(){
          	protected PasswordAuthentication getPasswordAuthentication() 
          	{
          	  //Is a password available
                if (password.isEmpty() || password == null) {  
              	String pwd = JOptionPane.showInputDialog(null, "Bitte Emailpasswort eingeben: ", "Matti", JOptionPane.PLAIN_MESSAGE);
              	properties.setProperty("mail.smtp.password", pwd);
                }else {
          		properties.setProperty("mail.smtp.password", password);	
                }
          		return new PasswordAuthentication(user, properties.getProperty("mail.smtp.password"));
          	}
      		});

      try{
         // Create a default MimeMessage object.
         MimeMessage message = new MimeMessage(session);
         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         message.addRecipient(Message.RecipientType.TO,
                                  new InternetAddress(to));

         // Set Subject: header field
         message.setSubject(subject);

         // Now set the actual message
         message.setText(messageText);

         // Send message
         Transport.send(message);
         //System.out.println("Nachricht gesendet....");
         JOptionPane.showMessageDialog(null, "Emailversand erfolgreich.", "Matti", JOptionPane.PLAIN_MESSAGE);
      }catch (MessagingException mex) {
         mex.printStackTrace();
      }
   }
}