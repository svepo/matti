/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;


public class Helper {
		// liefert einen Teil-String ab einer bestimmten Position eines Trenner-Strings zurück
		//und ersetzt den Unterstrich durch ein Komma
	    public String cutFront(String txt, String teil, int number) {
	        for (int i = 0; i < number; i++) {
	            txt = txt.substring(txt.indexOf(teil) + 1, txt.length());
	           
	        }
	        txt = txt.replace("_","," );
	        String[] name = txt.split(",");
	        
	        return txt;
	    }
	    // liefer den Teil bis zum n-ten Vorkommen von teil zurück 
	    public String cutBack(String txt, String teil, int number) {
	        for (int i = 0; i < number; i++) {
	            txt = txt.substring(0, txt.lastIndexOf(teil));
	        }
	        return txt;
	    }
	    
	    public Date StringToDate(String dateString){
	    	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	    	Date date = null;
	     
	    	try {
	     
	    		date = formatter.parse(dateString);
	    		
	     
	    	} catch (ParseException e) {
	    		e.printStackTrace();
	    	}
	    	return date;
	    }
	    
	    public String convertDateToString(Date dateValue){
	    	 
	    	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	    	String dateString = df.format(dateValue);
	    	
	    	return dateString;
	    			
	    }
	    
	   
	 
}
