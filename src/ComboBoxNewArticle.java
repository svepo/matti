/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

public class ComboBoxNewArticle {

	private String articleName;
	private int articleNumber;
	
	public ComboBoxNewArticle(String articleName, int articleNumber){
        this.articleName = articleName;
        this.articleNumber = articleNumber;
    }
 
    public String getComboBoxNewArticleEntry(){
        return this.articleNumber + " - " + this.articleName;
    }
 
    public String getArticleName(){
    	return this.articleName;
    }
 
   
    public int getArticleNumber(){
    	return this.articleNumber;
    }

}
