/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;



public class MemberArticleTableModel extends AbstractTableModel{
	
	 private List articleList = new ArrayList();
	 
	 private String[] columnLabels = {"Leihartikel", "Leihart", "Leihe ab", "Status", "Leihe bis"};
	    
	 public void setArticleList(List list){
	    articleList = list;
	    }
	    
	 public int getColumnCount() {
	    return columnLabels.length;
	    }

	 public int getRowCount() {
	    return articleList.size();
	    }
	    
	    @Override
	 public String getColumnName(int column) {
	    return columnLabels[column];
	    }

	 public Object getValueAt(int row, int col) {
	        Artikel a = (Artikel)articleList.get(row);
	        switch (col) {
	        case 0:
	                return a.getArtikel();
	        case 1:
	                return a.getArtikelNummer();
	        case 2:
	                return a.getArtikelStock();
	        case 3:
	        		return a.getArtikel();
	        case 4:
	        		return a.getArtikelStock();
	        default:
	                return null;
	        }
	    
	    }
}
