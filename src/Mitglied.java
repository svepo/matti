/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

import java.util.Date;


public class Mitglied {
	
	String name;
	String vorname;
	String phone;
	String mail;
	int maxArtikel = 3;
	String leihartikel;
	int articleNumber;
	Date leihDate;
	Date ausgabeDate;
	String leihArt;
	
	public void mitglied() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
	public int getMaxArtikel() {
		return maxArtikel;
	}

	public void setMaxArtikel(int maxArtikel) {
		this.maxArtikel = maxArtikel;
	}

	public String getLeihartikel() {
		return leihartikel;
	}

	public void setLeihartikel(String leihartikel) {
		this.leihartikel = leihartikel;
	}

	public int getArticleNumber() {
		return articleNumber;
	}

	public void setArticleNumber(int articleNumber) {
		this.articleNumber = articleNumber;
	}

	public Date getLeihDate() {
		return leihDate;
	}

	public void setLeihDate(Date leihDate) {
		this.leihDate = leihDate;
	}

	public Date getAusgabeDate() {
		return ausgabeDate;
	}

	public void setAusgabeDate(Date ausgabeDate) {
		this.ausgabeDate = ausgabeDate;
	}

	public String getLeihArt() {
		return leihArt;
	}

	public void setLeihArt(String leihArt) {
		this.leihArt = leihArt;
	}

}
