/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Prefs {
	
	String rent_1;
	String rent_2;
	String rent_3;
	int defaultMaxArtikel;
	String email_from;
	String email_host;
	String email_port;
	String email_user;
	String email_password;
	Boolean email_startTls;
	Boolean email_smtpAuth;
	String email_subjectLn;
	String email_messageTextLn;
	String email_subjectLz;
	String email_messageTextLz;
	Boolean first_Run;
	String path_To_DB;
	
	
	
	public String getRent_1() {
		return rent_1;
	}



	public void setRent_1(String rent_1) {
		this.rent_1 = rent_1;
	}



	public String getRent_2() {
		return rent_2;
	}



	public void setRent_2(String rent_2) {
		this.rent_2 = rent_2;
	}



	public String getRent_3() {
		return rent_3;
	}



	public void setRent_3(String rent_3) {
		this.rent_3 = rent_3;
	}



	public int getDefaultMaxArtikel() {
		return defaultMaxArtikel;
	}



	public void setDefaultMaxArtikel(int defaultMaxArtikel) {
		this.defaultMaxArtikel = defaultMaxArtikel;
	}



	public String getEmail_from() {
		return email_from;
	}



	public void setEmail_from(String email_from) {
		this.email_from = email_from;
	}



	public String getEmail_host() {
		return email_host;
	}



	public void setEmail_host(String email_host) {
		this.email_host = email_host;
	}



	public String getEmail_port() {
		return email_port;
	}



	public void setEmail_port(String email_port) {
		this.email_port = email_port;
	}



	public String getEmail_user() {
		return email_user;
	}



	public void setEmail_user(String email_user) {
		this.email_user = email_user;
	}



	public String getEmail_password() {
		return email_password;
	}



	public void setEmail_password(String email_password) {
		this.email_password = email_password;
	}



	public Boolean getEmail_startTls() {
		return email_startTls;
	}



	public void setEmail_startTls(Boolean email_startTls) {
		this.email_startTls = email_startTls;
	}



	public Boolean getEmail_smtpAuth() {
		return email_smtpAuth;
	}



	public void setEmail_smtpAuth(Boolean email_smtpAuth) {
		this.email_smtpAuth = email_smtpAuth;
	}



	public String getEmail_subjectLn() {
		return email_subjectLn;
	}



	public void setEmail_subjectLn(String email_subjectLn) {
		this.email_subjectLn = email_subjectLn;
	}

	public String getEmail_subjectLz() {
		return email_subjectLz;
	}



	public void setEmail_subjectLz(String email_subjectLz) {
		this.email_subjectLz = email_subjectLz;
	}


	public String getEmail_messageTextLn() {
		return email_messageTextLn;
	}



	public void setEmail_messageTextLn(String email_messageTextLn) {
		this.email_messageTextLn = email_messageTextLn;
	}

	public String getEmail_messageTextLz() {
		return email_messageTextLz;
	}



	public void setEmail_messageTextLz(String email_messageTextLz) {
		this.email_messageTextLz = email_messageTextLz;
	}


	public String getPath_To_DB() {
		return path_To_DB;
	}



	public void setPath_To_DB(String path_To_DB) {
		this.path_To_DB = path_To_DB;
	}
	
	public Boolean getFirst_Run() {
		return first_Run;
	}



	public void setFirst_Run(Boolean first_Run) {
		this.first_Run = first_Run;
	}



	public void saveParamChangesAsXML (String [] mattiProps) {
	   
		try {
	        Properties props = new Properties();
	        props.setProperty("RENT_1", mattiProps[0]);
	        props.setProperty("RENT_2", mattiProps[1]);
	        props.setProperty("RENT_3", mattiProps[2]);
	        props.setProperty("DEFAULT_MAX_ARTIKEL", String.valueOf(mattiProps[3]));
	        props.setProperty("EMAIL_FROM", mattiProps[4]);
	        props.setProperty("EMAIL_HOST", mattiProps[5]);
	        props.setProperty("EMAIL_PORT", mattiProps[6]);
	        props.setProperty("EMAIL_USER", mattiProps[7]);
	        props.setProperty("EMAIL_PASSWD", mattiProps[8]);
	        props.setProperty("EMAIL_STARTTLS", String.valueOf(mattiProps[9]));
	        props.setProperty("EMAIL_SMTPAUTH", String.valueOf(mattiProps[10]));
	        props.setProperty("EMAIL_SUBJECTLN", mattiProps[13]);
	        props.setProperty("EMAIL_MESSAGETEXTLN", mattiProps[14]);
	        props.setProperty("EMAIL_SUBJECTLZ", mattiProps[15]);
	        props.setProperty("EMAIL_MESSAGETEXTLZ", mattiProps[16]);
	        props.setProperty("PATH_TO_DB", mattiProps[11]);
	        props.setProperty("FIRST_RUN", mattiProps[12]);
	        File f = new File("matti.ini");
	        OutputStream out = new FileOutputStream( f );
	        props.storeToXML(out, "Matti Properties");
	        //System.out.println("Einstellungen geändert!");
	    }
	    catch (Exception e ) {
	        e.printStackTrace();
	    }
	}
}
