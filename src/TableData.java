/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

import java.sql.ResultSet;


public class TableData {
	
	Object [][] data;
	String ausgabeLeihartikel;
	String ausgabeLeihart;
	String ausgabeLeihDate;
	int id;
	Boolean ausgabeReturned;
	String ausgabeIsReturned;
	String ausgabeReturnDate;
	public String getAusgabeLeihartikel() {
		return ausgabeLeihartikel;
	}
	public void setAusgabeLeihartikel(String ausgabeLeihartikel) {
		this.ausgabeLeihartikel = ausgabeLeihartikel;
	}
	public String getAusgabeLeihart() {
		return ausgabeLeihart;
	}
	public void setAusgabeLeihart(String ausgabeLeihart) {
		this.ausgabeLeihart = ausgabeLeihart;
	}
	public String getAusgabeLeihDate() {
		return ausgabeLeihDate;
	}
	public void setAusgabeLeihDate(String ausgabeLeihDate) {
		this.ausgabeLeihDate = ausgabeLeihDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Boolean getAusgabeReturned() {
		return ausgabeReturned;
	}
	public void setAusgabeReturned(Boolean ausgabeReturned) {
		this.ausgabeReturned = ausgabeReturned;
	}
	public String getAusgabeIsReturned() {
		return ausgabeIsReturned;
	}
	public void setAusgabeIsReturned(String ausgabeIsReturned) {
		this.ausgabeIsReturned = ausgabeIsReturned;
	}
	public String getAusgabeReturnDate() {
		return ausgabeReturnDate;
	}
	public void setAusgabeReturnDate(String ausgabeReturnDate) {
		this.ausgabeReturnDate = ausgabeReturnDate;
	}
	
	public Object[][] getTableData(ResultSet tableRS){
		
		data = new Object[][]{
				{"a","b","c","d","e"},
				{"f","g","h","i","j"}
		};
		return data;
	}

}
