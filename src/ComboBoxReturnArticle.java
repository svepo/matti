/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

public class ComboBoxReturnArticle{
	
	private String articleName;
	private String rentDate;
	private int articleID;
	
	public ComboBoxReturnArticle(String articleName, String rentDate, int articleID){
        this.articleName = articleName;
        this.rentDate = rentDate;
        this.articleID = articleID;
    }
 
    public String getComboBoxReturnArticleEntry(){
        return this.articleName + " -> " + this.rentDate;
    }
 
    public String getArticleName(){
    	return this.articleName;
    }
 
    public String getRentDate(){ 
    	return this.rentDate;
    }
    public int getArticleID(){
    	return this.articleID;
    }

}
