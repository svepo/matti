/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JPanel;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import javax.swing.UIManager;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;

import java.awt.Dimension;

import javax.swing.JTextPane;
import javax.swing.JComboBox;

import org.apache.commons.lang3.StringUtils;

import java.awt.CardLayout;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JFormattedTextField;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
/**
 * @author Sven Powalla
 *
 */
public class MattiGUI {
	//main class
	private JFrame frame;
	final DefaultTableModel model = new DefaultTableModel( 0, 0 );
	static String [] mattiProperties = new String[17];
	static String rental1;
	static String rental2;
	static String rental3;
	static int defaultMaxLeihartikel;
	static String emailFrom;
	static String emailHost;
	static String emailPort;
	static String emailUser;
	static String emailPassword;
	static Boolean emailStartTLS;
	static Boolean emailSmtpAuth;
	static String emailSubjectLn;
	static String emailMessageTextLn;
	static String emailSubjectLz;
	static String emailMessageTextLz;
	static String dbPath;
	static Boolean firstRun;
	static Boolean newArticleFlag;
	static String table;
	static Boolean isNewMember;
	
	private int eingabeID;
	private CardLayout cl;
	private JPanel panelMitgliedAbfrage;
	private JTextField textFieldMailFrom;
	private JTextField textFieldHost;
	private JTextField textFieldPort;
	private JTextField textFieldUser;
	private JTextField textFieldPassword;
	private JTextField textFieldDbPath;
	private JTextField textFieldLeihart1;
	private JTextField textFieldLeihart2;
	private JTextField textFieldLeihart3;
	private JTextField textFieldDefaultMaxLeihartikel;
	private JTextField textFieldSubjectLn;
	private JTextArea textAreaMessageLn;
	private JTextField textFieldSubjectLz;
	private JTextArea textAreaMessageLz;
	private JTextField textFieldBezeichnung;
	private JTextField textFieldArtikelNr;
	private JTextField textFieldBestand;
	private JTextField textFieldBestandAvailable;
	private JFormattedDateTextField fTextFieldDate;
	private JCheckBox ckbxEnable = new JCheckBox("Enable");
	private static Prefs writePrefs = new Prefs();
	private static Mitglied mitglied = new Mitglied();
	
	private JPanel panelPrefs;
	private static Database db = new Database();
	private JTextField textFieldFirstName;
	private JTextField textFieldLastName;
	private JTextField textFieldPhone;
	private JTextField textFieldMail;
	private JTextField textFieldMaxArticle;
	private JTable tableMemberArticles;
	private JTextField textFieldSelectedMember;
	
	private JComboBox comboBoxMemberChooserMember; 
	private JButton btnMitgliedLschen;
	private JButton btnMitgliedNeu;
	private JButton btnMitgliedSpeichern;
	private JButton btnArticelNew;
	private JButton btnArtikelOK;
	private JButton btnLeiheNeu;
	private JButton btnLeiheZurck;
	private ArrayList cbList;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		loadParams();
		if (firstRun == true) {
			
			int result = JOptionPane.showConfirmDialog((Component) null, "Dies ist der erste Start von Matti\n...erstelle alle Dateien!",
			        "Matti", JOptionPane.OK_CANCEL_OPTION);
			if (result == 0) {
				FirstRun();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							MattiGUI window = new MattiGUI();
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} else {
				System.exit(1);
			}
		} else {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MattiGUI window = new MattiGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		}
	}

	/**
	 * Create the application.
	 * @throws ParseException 
	 * @throws FileNotFoundException 
	 * @throws InterruptedException 
	 */
	public MattiGUI() throws ParseException, FileNotFoundException, InterruptedException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws ParseException 
	 * @throws FileNotFoundException 
	 * @throws InterruptedException 
	 * @wbp.parser.entryPoint
	 */
	@SuppressWarnings("rawtypes")
	private void initialize() throws ParseException, FileNotFoundException, InterruptedException {
		
		final HashMap comboMap = new HashMap();
		
		Font defaultFont = (Font)UIManager.get("messageFont");
	    UIManager.put("OptionPane.messageFont", new Font("Dialog",Font.PLAIN,12));

	    Font defaultColor = (Font)UIManager.get("messageForeground");
	    UIManager.put("OptionPane.messageForeground", Color.DARK_GRAY);
		
		frame = new JFrame();
		
		//die Grösse des Programfensters beim Start -> bei Bedarf ändern
		frame.setSize(new Dimension(1000, 800));
		frame.getContentPane().setFont(new Font("Courier 10 Pitch", Font.PLAIN, 12));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		
		cl = new CardLayout();
		final JPanel panelMain = new JPanel(cl);
		panelMain.setAlignmentY(Component.TOP_ALIGNMENT);
		frame.getContentPane().add(panelMain, BorderLayout.CENTER);
		
		cl.show(panelMain, "Willkommen zu Matti");
		
		/* Buttons für das obere Panel*/
		
		JButton btnMitgliedAbfrage = new JButton("Abfrage");
		btnMitgliedAbfrage.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnMitgliedAbfrage.setHorizontalTextPosition(SwingConstants.CENTER);
		btnMitgliedAbfrage.setToolTipText("Mitglied Abfrage");
		btnMitgliedAbfrage.setIcon(new ImageIcon("./icons/icons48/request48.png"));
		btnMitgliedAbfrage.setFont(UIManager.getFont("FormattedTextField.font"));
		btnMitgliedAbfrage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				cl.show(panelMain, "Mitglied Abfrage");
				
			}
		});
		panel.add(btnMitgliedAbfrage);
		
		JButton btnMitgliedVerwalten = new JButton("Mitglied");
		btnMitgliedVerwalten.setToolTipText("Mitglieder verwalten");
		btnMitgliedVerwalten.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnMitgliedVerwalten.setHorizontalTextPosition(SwingConstants.CENTER);
		btnMitgliedVerwalten.setIcon(new ImageIcon("./icons/icons48/members48.png"));
		btnMitgliedVerwalten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				cl.show(panelMain, "Mitglied Neu");
			}
		});
		btnMitgliedVerwalten.setFont(UIManager.getFont("FormattedTextField.font"));
		panel.add(btnMitgliedVerwalten);
		
		JButton btnLeihartverwalten = new JButton("Leihartikel");
		btnLeihartverwalten.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnLeihartverwalten.setHorizontalTextPosition(SwingConstants.CENTER);
		btnLeihartverwalten.setIcon(new ImageIcon("./icons/icons48/article48.png"));
		btnLeihartverwalten.setToolTipText("Leihartikel verwalten");
		btnLeihartverwalten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				cl.show(panelMain, "Leihartikel verwalten");	
			}
		});
		btnLeihartverwalten.setFont(UIManager.getFont("FormattedTextField.font"));
		panel.add(btnLeihartverwalten);
		
		JButton btnEinstellungen = new JButton("Einstellungen");
		btnEinstellungen.setToolTipText("Einstellungen");
		btnEinstellungen.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnEinstellungen.setHorizontalTextPosition(SwingConstants.CENTER);
		btnEinstellungen.setIcon(new ImageIcon("./icons/icons48/preferences48.png"));
		btnEinstellungen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				cl.show(panelMain, "Voreinstellungen");
				
				writePrefs.setEmail_from(emailFrom);
				writePrefs.setEmail_host(emailHost);
				writePrefs.setEmail_port(emailPort);
				writePrefs.setEmail_user(emailUser);
				writePrefs.setEmail_password(emailPassword);
				writePrefs.setEmail_startTls(emailStartTLS);
				writePrefs.setEmail_smtpAuth(emailSmtpAuth);
				writePrefs.setEmail_subjectLn(emailSubjectLn);
				writePrefs.setEmail_messageTextLn(emailMessageTextLn);
				writePrefs.setEmail_subjectLz(emailSubjectLz);
				writePrefs.setEmail_messageTextLz(emailMessageTextLz);
				writePrefs.setPath_To_DB(dbPath);
				writePrefs.setRent_1(rental1);
				writePrefs.setRent_2(rental2);
				writePrefs.setRent_3(rental3);
				writePrefs.setDefaultMaxArtikel(defaultMaxLeihartikel);
				
				
			}
		});
		btnEinstellungen.setFont(UIManager.getFont("FormattedTextField.font"));
		panel.add(btnEinstellungen);
		
		JButton btnHilfe = new JButton("Hilfe");
		btnHilfe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				cl.show(panelMain, "Hilfe");
			}
		});
		btnHilfe.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnHilfe.setHorizontalTextPosition(SwingConstants.CENTER);
		btnHilfe.setIcon(new ImageIcon("./icons/icons48/help48.png"));
		btnHilfe.setToolTipText("Hilfe");
		btnHilfe.setFont(UIManager.getFont("FormattedTextField.font"));
		panel.add(btnHilfe);
		
		JButton btnExitButton = new JButton("Exit");
		btnExitButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnExitButton.setHorizontalTextPosition(SwingConstants.CENTER);
		btnExitButton.setIcon(new ImageIcon("./icons/icons48/exit48.png"));
		btnExitButton.setToolTipText("Program beenden");
		btnExitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnExitButton.setFont(UIManager.getFont("FormattedTextField.font"));
		panel.add(btnExitButton);
		
		Component horizontalStrut = Box.createHorizontalStrut(150);
		horizontalStrut.setMaximumSize(new Dimension(350, 32767));
		panel.add(horizontalStrut);
		
		
		/* Hier sind die Panels für panelMain*/
		
		/*Alle Komponenten für Matti Start */
		/*Dieses Panel wird ausschlieslich beim Start angezeigt*/
		final JPanel panelMattiStart = new JPanel();
		panelMattiStart.setBorder(new EtchedBorder(EtchedBorder.LOWERED, Color.WHITE, Color.LIGHT_GRAY));
		panelMain.add(panelMattiStart, "Willkommen zu Matti");
		panelMattiStart.setLayout(null);
		
		JLabel lblMattiStart = new JLabel("");
		Thread.sleep(1000);
		lblMattiStart.setIcon(new ImageIcon(MattiGUI.class.getResource("/pictures/splash-516x377_ver1015.png")));
		
		lblMattiStart.setFont(UIManager.getFont("FormattedTextField.font"));
		lblMattiStart.setBounds(300, 150, 516, 377);
		panelMattiStart.add(lblMattiStart);
		
		/*Alle Komponenten für Mitglied Abfrage */
		final JPanel panelMitgliedAbfrage = new JPanel();
		panelMitgliedAbfrage.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentHidden(ComponentEvent e) {
				resetComponentPanelAbfrage();
			}
		});
		panelMitgliedAbfrage.setBorder(new EtchedBorder(EtchedBorder.LOWERED, Color.WHITE, Color.LIGHT_GRAY));
		panelMain.add(panelMitgliedAbfrage, "Mitglied Abfrage");
		panelMitgliedAbfrage.setLayout(null);
		
		
		final JPanel panelMitgliedAbfrageNeu = new JPanel();
		panelMitgliedAbfrageNeu.setBounds(22, 448, 600, 200);
		panelMitgliedAbfrageNeu.setVisible(false);
		panelMitgliedAbfrage.add(panelMitgliedAbfrageNeu);
		panelMitgliedAbfrageNeu.setLayout(null);
		
		final JPanel panelMitgliedAbfrageDelete = new JPanel();
		panelMitgliedAbfrageDelete.setBounds(22, 448, 600, 200);
		panelMitgliedAbfrageDelete.setVisible(false);
		panelMitgliedAbfrage.add(panelMitgliedAbfrageDelete);
		panelMitgliedAbfrageDelete.setLayout(null);
	
		
		JLabel lblTitleMemberRequest = new JLabel("Abfrage");
		lblTitleMemberRequest.setForeground(UIManager.getColor("List.dropLineColor"));
		lblTitleMemberRequest.setFont(UIManager.getFont("Button.font"));
		lblTitleMemberRequest.setBounds(22, 25, 140, 25);
		panelMitgliedAbfrage.add(lblTitleMemberRequest);
		
		JLabel lblMitglied = new JLabel("Mitglied");
		lblMitglied.setForeground(Color.DARK_GRAY);
		lblMitglied.setFont(UIManager.getFont("FormattedTextField.font"));
		lblMitglied.setBounds(22, 70, 160, 15);
		panelMitgliedAbfrage.add(lblMitglied);
		
		btnLeiheNeu = new JButton("Leihe neu");
		btnLeiheZurck = new JButton("Leihe zurück");
		
		final JComboBox comboBoxMemberChooser = new JComboBox();
		SortedComboBoxModel memberChooserModel = new SortedComboBoxModel(db.getTables("MITGLIEDER", 1));
		comboBoxMemberChooser.setModel(memberChooserModel);
		comboBoxMemberChooser.setSelectedIndex(-1);
		comboBoxMemberChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//Ermittle die Member-Tabelle
				ArrayList<Integer> memberIndexList;
				
				memberIndexList = getIndex(db.getTables("MITGLIEDER",1));	
				table = showMember(memberIndexList.indexOf(comboBoxMemberChooser.getSelectedIndex()));
				tableMemberArticles.removeAll();
				tableMemberArticles.updateUI();
				
				//Gebe Mitgliedsdaten aus
				textFieldSelectedMember.setText(mitglied.getName()+", "+mitglied.getVorname()+" Tel: "+mitglied.getPhone()+" Mail: "+mitglied.getMail());
				
				//LeiheNeu -Button kann nun betätigt werden
				btnLeiheNeu.setEnabled(true);
				
			
				//Ermittle das passende Resultset
				ResultSet memberRS = null;
				
				memberRS = readMemberArticleData(table);
				
				if (memberRS != null){
					
					//weise der Tabelle das Model zu
				
					tableMemberArticles.setModel(resultSet2TableModel(memberRS));
					
					tableMemberArticles.getColumnModel().getColumn(0).setPreferredWidth(25);
					tableMemberArticles.getColumnModel().getColumn(4).setPreferredWidth(25);
					tableMemberArticles.getColumnModel().getColumn(3).setPreferredWidth(25);
					
					//LeiheZurück - Button aktivieren
					btnLeiheZurck.setEnabled(true);
			
				} else {
					
					tableMemberArticles.setModel(emptyModel());
					//JOptionPane.showMessageDialog(null, "Keine Daten vorhanden.",
					  //      "Matti", JOptionPane.OK_OPTION);
					
				}
				
			}
		});
		comboBoxMemberChooser.setForeground(Color.DARK_GRAY);
		comboBoxMemberChooser.setFont(UIManager.getFont("FormattedTextField.font"));
		comboBoxMemberChooser.setBounds(180, 68, 250, 25);
		panelMitgliedAbfrage.add(comboBoxMemberChooser);
		
		
		
		final JComboBox comboBoxLeihartikelChooser = new JComboBox(db.getArticleArray("ARTIKEL",0));
		comboBoxLeihartikelChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				mitglied.setLeihartikel((String) comboBoxLeihartikelChooser.getSelectedItem());
			}
		});
		comboBoxLeihartikelChooser.setBounds(158, 0, 250, 25);
		comboBoxLeihartikelChooser.setFont(UIManager.getFont("FormattedTextField.font"));
		panelMitgliedAbfrageNeu.add(comboBoxLeihartikelChooser);
		
		//Variable initialisieren
		final Boolean isReturned = false;
		
		btnLeiheNeu.setFont(UIManager.getFont("FormattedTextField.font"));
		btnLeiheNeu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				panelMitgliedAbfrageNeu.setVisible(true);
				panelMitgliedAbfrageDelete.setVisible(false);
				btnLeiheZurck.setEnabled(false);
				
				//Prüfe ob maximale Leihartikelzahl erreicht ist
				
				int anzArt = db.countNotReturnedRows(table);
				
				if (anzArt > mitglied.getMaxArtikel()) {
					JOptionPane.showMessageDialog(null,"Maximale Leihartikelanzahl erreicht!"
							, "Matti", JOptionPane.WARNING_MESSAGE);
					
					
				} else {
					
					panelMitgliedAbfrageNeu.setVisible(true);
					panelMitgliedAbfrageDelete.setVisible(false);
					
				}
			}
		});
		
		btnLeiheNeu.setForeground(Color.DARK_GRAY);
		btnLeiheNeu.setBounds(22, 400, 130, 25);
		panelMitgliedAbfrage.add(btnLeiheNeu);
		btnLeiheNeu.setEnabled(false);
		
		final ReturnArticleMutableModel comboBoxReturnArticleModel = new ReturnArticleMutableModel();
		final JComboBox comboBoxLeihartikelReturnChooser = new JComboBox(comboBoxReturnArticleModel);
		
		btnLeiheZurck.setFont(UIManager.getFont("FormattedTextField.font"));
		btnLeiheZurck.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent arg0) {
				//Andere Elemente deaktivieren
				panelMitgliedAbfrageNeu.setVisible(false);
				panelMitgliedAbfrageDelete.setVisible(true);
				btnLeiheNeu.setEnabled(false);
				
				//HashMap leeren
				comboMap.clear();
			
				//ComboBox Einträge erstellen
				panelMitgliedAbfrageDelete.add(comboBoxLeihartikelReturnChooser);		
				for (int i = 0; i < comboBoxReturnArticleModel.getSize(); i++) {
					comboBoxReturnArticleModel.removeElementAt(i);
				}
				String[] comboStr = new String[db.readFromTableAndStoreIntoArray(table,0).length];
				String[] comboDateStr = new String[db.readFromTableAndStoreIntoArray(table,0).length];
				String[] idStr = new String[db.readFromTableAndStoreIntoArray(table,0).length];
				String[] ausgabeArtikel = new String[3];
    			
				ReturnArticleMutableModel cbModel = new ReturnArticleMutableModel(); 
				comboBoxLeihartikelReturnChooser.setModel(cbModel);
				comboStr = db.readFromTableAndStoreIntoArray(table,0);
				comboDateStr = db.readFromTableAndStoreIntoArray(table,1);
				idStr = db.readFromTableAndStoreIntoArray(table,2);
				for (int i = 0; i < db.readFromTableAndStoreIntoArray(table,0).length; i++) {
					
					ausgabeArtikel = db.readArticleRow("ARTIKEL", Integer.parseInt(comboStr[i]));
					comboBoxLeihartikelReturnChooser.addItem(new ComboBoxReturnArticle(ausgabeArtikel[1],comboDateStr[i],Integer.parseInt(idStr[i])));
					
					comboMap.put(i+1, new Integer (Integer.parseInt(idStr[i])));
				}
				comboBoxLeihartikelReturnChooser.updateUI();		
			}
		});
		btnLeiheZurck.setForeground(Color.DARK_GRAY);
		btnLeiheZurck.setBounds(180, 400, 130, 25);
		panelMitgliedAbfrage.add(btnLeiheZurck);
		btnLeiheZurck.setEnabled(false);
		
		
		comboBoxLeihartikelReturnChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
			}
		});
		comboBoxLeihartikelReturnChooser.setBounds(158, 0, 250, 25);
		comboBoxLeihartikelReturnChooser.setFont(UIManager.getFont("FormattedTextField.font"));		
		
		//ScrollPane für die Table
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 160, 800, 200);
		panelMitgliedAbfrage.add(scrollPane);
		tableMemberArticles = new JTable();
		
		scrollPane.setViewportView(tableMemberArticles);
		tableMemberArticles.setBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), Color.LIGHT_GRAY));
		tableMemberArticles.setForeground(Color.DARK_GRAY);
		
		textFieldSelectedMember = new JTextField();
		textFieldSelectedMember.setBorder(null);
		textFieldSelectedMember.setForeground(Color.DARK_GRAY);
		textFieldSelectedMember.setEditable(false);
		textFieldSelectedMember.setBackground(UIManager.getColor("Button.background"));
		textFieldSelectedMember.setBounds(22, 130, 410, 20);
		panelMitgliedAbfrage.add(textFieldSelectedMember);
		textFieldSelectedMember.setColumns(10);
		
		final JLabel lblLeihartikel = new JLabel("Leihartikel");
		lblLeihartikel.setBounds(0, 2, 160, 15);
		panelMitgliedAbfrageNeu.add(lblLeihartikel);
		lblLeihartikel.setForeground(Color.DARK_GRAY);
		lblLeihartikel.setFont(UIManager.getFont("FormattedTextField.font"));
		
		final JLabel lblLeihartikelReturn = new JLabel("Leihartikel");
		lblLeihartikelReturn.setBounds(0, 2, 160, 15);
		panelMitgliedAbfrageDelete.add(lblLeihartikelReturn);
		lblLeihartikelReturn.setForeground(Color.DARK_GRAY);
		lblLeihartikelReturn.setFont(UIManager.getFont("FormattedTextField.font"));
		
		final JLabel lblRentDate = new JLabel("Leihdatum");
		lblRentDate.setBounds(0, 32, 160, 15);
		panelMitgliedAbfrageNeu.add(lblRentDate);
		lblRentDate.setForeground(Color.DARK_GRAY);
		lblRentDate.setFont(UIManager.getFont("FormattedTextField.font"));
		
		final JLabel lblReturnDate = new JLabel("Rückgabedatum");
		lblReturnDate.setBounds(0, 32, 160, 15);
		panelMitgliedAbfrageDelete.add(lblReturnDate);
		lblReturnDate.setForeground(Color.DARK_GRAY);
		lblReturnDate.setFont(UIManager.getFont("FormattedTextField.font"));
		
		final JFormattedDateTextField fTextFieldDate = new JFormattedDateTextField();
		fTextFieldDate.setForeground(Color.DARK_GRAY);
		fTextFieldDate.setFont(UIManager.getFont("FormattedTextField.font"));
		fTextFieldDate.setBounds(158, 30, 200, 20);
		fTextFieldDate.setValue(new Date()); 
		panelMitgliedAbfrageNeu.add(fTextFieldDate);
		
		final JFormattedDateTextField fTextFieldReturnDate = new JFormattedDateTextField();
		fTextFieldReturnDate.setForeground(Color.DARK_GRAY);
		fTextFieldReturnDate.setFont(UIManager.getFont("FormattedTextField.font"));
		fTextFieldReturnDate.setBounds(158, 30, 200, 20);
		fTextFieldReturnDate.setValue(new Date()); 
		panelMitgliedAbfrageDelete.add(fTextFieldReturnDate);
		
		final JLabel lblRentCondition = new JLabel("Leihart");
		lblRentCondition.setBounds(0, 62, 160, 15);
		panelMitgliedAbfrageNeu.add(lblRentCondition);
		lblRentCondition.setForeground(Color.DARK_GRAY);
		lblRentCondition.setFont(UIManager.getFont("FormattedTextField.font"));
		
		String[] rentalConditions = {rental1, rental2, rental3};
		final JComboBox comboBoxRentCondChooser = new JComboBox(rentalConditions);
		comboBoxRentCondChooser.setSelectedIndex(-1);
		comboBoxRentCondChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Leihart festlegen
				mitglied.setLeihArt((String) comboBoxRentCondChooser.getSelectedItem());
			}
		});
		comboBoxRentCondChooser.setBounds(158, 60, 200, 25);
		panelMitgliedAbfrageNeu.add(comboBoxRentCondChooser);
		comboBoxRentCondChooser.setForeground(Color.DARK_GRAY);
		comboBoxRentCondChooser.setFont(UIManager.getFont("FormattedTextField.font"));
		
		JButton btnSaveNewRent = new JButton("Speichern");
		btnSaveNewRent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int artNum = extractArtNum((String)comboBoxLeihartikelChooser.getSelectedItem(), "-");
				String[] selectedArt = new String[3];
				
				//ordne Leihartikelnummer einem Artikel zu    	
				selectedArt = db.readArticleRow("ARTIKEL", artNum);
				mitglied.setArticleNumber(artNum);
				mitglied.setLeihartikel(selectedArt[1]);
        	
				//Passe Bestand an
				int newStock = Integer.parseInt(selectedArt[2])-1;
				int oldStock = Integer.parseInt(selectedArt[2]);
				
				//Prüfe ob Bestand vorhanden
				if (oldStock == 0) {
					JOptionPane.showMessageDialog(null,"Kein Bestand von "+ mitglied.getLeihartikel()+" mehr vorhanden, bitte erst Bestand auffüllen!", "Matti", JOptionPane.WARNING_MESSAGE);
					}
				
				//Wandle die Datumseingabe um und übergebe sie dem Setter
				DateConverter dateString = new DateConverter();
				String dateStr = ""+fTextFieldDate.getValue();	
				mitglied.setLeihDate(dateString.toDate(dateStr));
			
				//Schreibe Datensatz
				try {
					db.updateArticleTable("ARTIKEL", newStock, artNum);
					db.writeToTable(table, mitglied.getArticleNumber(), mitglied.getLeihArt(), dateString.ToString((mitglied.getLeihDate())), isReturned);
					int selectSendMail = JOptionPane.showConfirmDialog(null, "Datensatz erfolgreich gespeichert.\nBestätigungsmail versenden?"
							, "Matti", JOptionPane.YES_NO_OPTION);
					
					if (selectSendMail == 0) {
						//Sende Email an Mitglied
						String mailSummary = searchAndReplace(emailMessageTextLz, 0);
						String mailSubject = searchAndReplace(emailSubjectLz, 0);
		
						SendMail sm = new SendMail();
						sm.Send(mailSubject, mailSummary, mitglied.getMail(), emailFrom, emailHost, emailUser, emailPassword, emailPort, emailStartTLS, emailSmtpAuth);
					}
					
					//Eingabepanel wieder sperren
					panelMitgliedAbfrageNeu.setVisible(false);

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			panelMitgliedAbfrage.revalidate();
			panelMitgliedAbfrage.repaint();
			}
		});
		btnSaveNewRent.setForeground(Color.DARK_GRAY);
		btnSaveNewRent.setFont(UIManager.getFont("FormattedTextField.font"));
		btnSaveNewRent.setBounds(0, 150, 115, 25);
		panelMitgliedAbfrageNeu.add(btnSaveNewRent);
		
		JButton btnSaveDeleteRent = new JButton("Speichern");
		btnSaveDeleteRent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String[] selectedStr = new String[4];
				int rowID = comboBoxLeihartikelReturnChooser.getSelectedIndex();
				//Liest den Datensatz zum ausgewählten Leihartikel
				selectedStr = db.readRow(table, (Integer)comboMap.get(rowID+1));
    			
    			String [] ausgabeArtikel = new String[3];
    			ausgabeArtikel = db.readArticleRow("ARTIKEL", Integer.parseInt(selectedStr[0]));
    			String ausgabeLeihartikel = ausgabeArtikel[1];
    			String ausgabeLeihart = selectedStr[1];
    			String ausgabeLeihdate = selectedStr[2];
    			
    			//Wandle die Datumseingabe um und übergebe sie dem Setter
				DateConverter dateString = new DateConverter();
				String dateStr = ""+fTextFieldReturnDate.getValue();
				String ausgabeDateStr = ""+ausgabeLeihdate;
				
				mitglied.setLeihartikel(ausgabeLeihartikel);
				mitglied.setLeihArt(ausgabeLeihart);
				mitglied.setLeihDate(dateString.toDate(dateStr));
				mitglied.setAusgabeDate(dateString.toDate(ausgabeDateStr));
				//Löschen bestätigen lassen
				int selectConfirm = JOptionPane.showConfirmDialog(null, "Datensatz: "+ausgabeLeihartikel+" als "+ausgabeLeihart+" vom "+ausgabeLeihdate+" bis "+dateStr+" löschen? "
						, "Matti", JOptionPane.YES_NO_OPTION);
				
				if (selectConfirm == 0) {
					//ISRETURNED auf true setzen
					Boolean returned = true;
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					
					try {
						//Flag auf Returned ändern, Datensatz aktualisieren
						db.isReturnedRecord(table, (Integer)comboMap.get(rowID+1), returned, sdf.format(mitglied.getLeihDate()));
						
						//Bestand wieder um 1 erhöhen
						db.updateArticleTable("ARTIKEL", Integer.parseInt(ausgabeArtikel[2])+1, Integer.parseInt(selectedStr[0]));
						
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				int selectMailConfirm = JOptionPane.showConfirmDialog(null, "Gelöscht!\nEmail versenden?"
						, "Matti", JOptionPane.YES_NO_OPTION);
				
				if (selectMailConfirm == 0) {
					//Sende Email an Mitglied
					String mailSummary = searchAndReplace(emailMessageTextLn, 1);
					String mailSubject = searchAndReplace(emailSubjectLn, 1);
					
					SendMail sm = new SendMail();
					sm.Send(mailSubject, mailSummary, mitglied.getMail(), emailFrom, emailHost, emailUser, emailPassword, emailPort, emailStartTLS, emailSmtpAuth);
				}
				//Hashmap leeren
				comboMap.clear();
				
				//Eingabepanel wieder sperren
				panelMitgliedAbfrageDelete.setVisible(false);

			}
		});
		btnSaveDeleteRent.setForeground(Color.DARK_GRAY);
		btnSaveDeleteRent.setFont(UIManager.getFont("FormattedTextField.font"));
		btnSaveDeleteRent.setBounds(0, 150, 115, 25);
		panelMitgliedAbfrageDelete.add(btnSaveDeleteRent);
		
				
		/*Alle Komponenten für Mitglied neu */
		JPanel panelMitgliedVerwalten = new JPanel();
		panelMitgliedVerwalten.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentHidden(ComponentEvent e) {
				resetComponentPanelMitglied();
			}
		});
		
		panelMitgliedVerwalten.setBorder(new EtchedBorder(EtchedBorder.LOWERED, Color.WHITE, Color.LIGHT_GRAY));
		panelMain.add(panelMitgliedVerwalten, "Mitglied Neu");
		panelMitgliedVerwalten.setLayout(null);
		
		JLabel lblTitleMemberNew = new JLabel("Mitglied");
		lblTitleMemberNew.setForeground(UIManager.getColor("List.dropLineColor"));
		lblTitleMemberNew.setBounds(22, 25, 140, 25);
		lblTitleMemberNew.setFont(UIManager.getFont("InternalFrame.titleFont"));
		panelMitgliedVerwalten.add(lblTitleMemberNew);
		
		JLabel lblFirstName = new JLabel("Vorname");
		lblFirstName.setForeground(Color.DARK_GRAY);
		lblFirstName.setBounds(22, 70, 160, 15);
		lblFirstName.setFont(UIManager.getFont("FormattedTextField.font"));
		panelMitgliedVerwalten.add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Nachname");
		lblLastName.setForeground(Color.DARK_GRAY);
		lblLastName.setFont(UIManager.getFont("FormattedTextField.font"));
		lblLastName.setBounds(22, 100, 160, 15);
		panelMitgliedVerwalten.add(lblLastName);
		
		JLabel lblTelefon = new JLabel("Telefon");
		lblTelefon.setForeground(Color.DARK_GRAY);
		lblTelefon.setFont(UIManager.getFont("FormattedTextField.font"));
		lblTelefon.setBounds(22, 130, 160, 15);
		panelMitgliedVerwalten.add(lblTelefon);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setForeground(Color.DARK_GRAY);
		lblEmail.setFont(UIManager.getFont("FormattedTextField.font"));
		lblEmail.setBounds(22, 160, 160, 15);
		panelMitgliedVerwalten.add(lblEmail);
		
		JLabel lblMaxLeihartikel = new JLabel("Max. Leihartikel");
		lblMaxLeihartikel.setForeground(Color.DARK_GRAY);
		lblMaxLeihartikel.setFont(UIManager.getFont("FormattedTextField.font"));
		lblMaxLeihartikel.setBounds(22, 190, 160, 15);
		panelMitgliedVerwalten.add(lblMaxLeihartikel);
		
		textFieldFirstName = new JTextField();
		textFieldFirstName.setForeground(Color.DARK_GRAY);
		textFieldFirstName.setBounds(180, 70, 300, 20);
		panelMitgliedVerwalten.add(textFieldFirstName);
		textFieldFirstName.setColumns(10);
		textFieldFirstName.setEditable(false);
		
		textFieldLastName = new JTextField();
		textFieldLastName.setForeground(Color.DARK_GRAY);
		textFieldLastName.setBounds(180, 100, 300, 20);
		panelMitgliedVerwalten.add(textFieldLastName);
		textFieldLastName.setColumns(10);
		textFieldLastName.setEditable(false);
		
		textFieldPhone = new JTextField();
		textFieldPhone.setForeground(Color.DARK_GRAY);
		textFieldPhone.setBounds(180, 130, 300, 20);
		panelMitgliedVerwalten.add(textFieldPhone);
		textFieldPhone.setColumns(10);
		textFieldPhone.setEditable(false);
		
		textFieldMail = new JTextField();
		textFieldMail.setForeground(Color.DARK_GRAY);
		textFieldMail.setBounds(180, 160, 300, 20);
		panelMitgliedVerwalten.add(textFieldMail);
		textFieldMail.setColumns(10);
		textFieldMail.setEditable(false);
		
		textFieldMaxArticle = new JTextField();
		textFieldMaxArticle.setForeground(Color.DARK_GRAY);
		textFieldMaxArticle.setBounds(180, 190, 300, 20);
		panelMitgliedVerwalten.add(textFieldMaxArticle);
		textFieldMaxArticle.setColumns(10);
		textFieldMaxArticle.setText(String.valueOf(defaultMaxLeihartikel));
		textFieldMaxArticle.setEditable(false);
		
		final JComboBox comboBoxMemberChooserMember = new JComboBox();
		SortedComboBoxModel cbMemberModel = new SortedComboBoxModel(db.getTables("MITGLIEDER",1));
		comboBoxMemberChooserMember.setModel(cbMemberModel);
		comboBoxMemberChooserMember.setSelectedIndex(-1);
		comboBoxMemberChooserMember.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent arg0) {
			
			ArrayList<Integer> indexList;
			
			indexList = getIndex(db.getTables("MITGLIEDER",1));	
			int j = indexList.indexOf(comboBoxMemberChooserMember.getSelectedIndex());
			
			showMember(j);
			textFieldFirstName.setText(mitglied.getVorname());
			textFieldFirstName.setEditable(true);
			textFieldLastName.setText(mitglied.getName());
			textFieldLastName.setEditable(true);
			textFieldPhone.setText(mitglied.getPhone());
			textFieldPhone.setEditable(true);
			textFieldMail.setText(mitglied.getMail());
			textFieldMail.setEditable(true);
			textFieldMaxArticle.setEditable(true);
			
			btnMitgliedSpeichern.setEnabled(true);
			btnMitgliedLschen.setEnabled(true);
			
			isNewMember = false;
				
			}
		});
		comboBoxMemberChooserMember.setForeground(Color.DARK_GRAY);
		comboBoxMemberChooserMember.setFont(UIManager.getFont("FormattedTextField.font"));
		comboBoxMemberChooserMember.setBounds(550, 68, 250, 25);
		panelMitgliedVerwalten.add(comboBoxMemberChooserMember);
		
		btnMitgliedSpeichern = new JButton("Mitglied speichern");
		btnMitgliedLschen = new JButton("Mitglied löschen");
		btnMitgliedNeu = new JButton("Mitglied neu");
		btnMitgliedNeu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				//btn deaktivieren
				btnMitgliedLschen.setEnabled(false);
				btnMitgliedSpeichern.setEnabled(false);
				
				//alle Felder initialisieren
				textFieldFirstName.setText("");
				textFieldFirstName.setEditable(true);
				textFieldLastName.setText("");
				textFieldLastName.setEditable(true);
				textFieldPhone.setText("");
				textFieldPhone.setEditable(true);
				textFieldMail.setText("");
				textFieldMail.setEditable(true);
				textFieldMaxArticle.setText(String.valueOf(defaultMaxLeihartikel));
				textFieldMaxArticle.setEditable(true);
				
				isNewMember = true;
			}
		});
		btnMitgliedNeu.setBounds(180, 230, 170, 25);
		btnMitgliedNeu.setFont(UIManager.getFont("FormattedTextField.font"));
		panelMitgliedVerwalten.add(btnMitgliedNeu);
		
		btnMitgliedLschen.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent arg0) {
				//btn Neu deaktivieren
				btnMitgliedNeu.setEnabled(false);
				btnMitgliedSpeichern.setEnabled(false);
				
				//Ermitteln des selektierten Mitglieds
				ArrayList<Integer> indexList;
				
				indexList = getIndex(db.getTables("MITGLIEDER",1));	
				int j = indexList.indexOf(comboBoxMemberChooserMember.getSelectedIndex());
            	String[] selectedMitglied = db.returnTable(j+1,"MITGLIEDER");
            	mitglied.setName(selectedMitglied[0]);
            	mitglied.setVorname(selectedMitglied[1]);
            	String table = selectedMitglied[2];
            	
            	//Prüfen ob alle Leihartikel zurückgegeben wurden, sonst Eintrag löschen
            	int anzArt = db.countNotReturnedRows(table);
            	
            	if (anzArt >= 1) {
					
					JOptionPane.showConfirmDialog(null,"Mitglied "+mitglied.getName()+","+mitglied.getVorname()+" hat noch offene Leihartikel! Bitte erst zurücknehmen.",
					        "Matti", JOptionPane.OK_OPTION);
				}else {
					int result = JOptionPane.showConfirmDialog(null,"Gewähltes Mitglied:\n"+mitglied.getName()+","+mitglied.getVorname()+" wirklich löschen?",
					        "Matti", JOptionPane.OK_CANCEL_OPTION);
					if (result == 0) {
						db.deleteMember(table,mitglied.getName(),mitglied.getVorname());
						String name = mitglied.getName()+", "+mitglied.getVorname();
						//Eintrag aus CB entfernen
						comboBoxMemberChooserMember.removeItem(name);
						comboBoxMemberChooserMember.updateUI();
						comboBoxMemberChooser.removeItem(name);
						comboBoxMemberChooser.updateUI();
						JOptionPane.showMessageDialog(null,"Mitglied gelöscht!",
						        "Matti", JOptionPane.OK_OPTION);
					}
					
				}
            	
            	//btn wieder aktivieren
            	btnMitgliedNeu.setEnabled(true);
			}
		});
		btnMitgliedLschen.setBounds(360, 230, 170, 25);
		btnMitgliedLschen.setFont(UIManager.getFont("FormattedTextField.font"));
		panelMitgliedVerwalten.add(btnMitgliedLschen);
		
		btnMitgliedSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {		
				//prüfen ob es ein neues Mitglied ist
				if (isNewMember) {
					Mitglied mitglied = new Mitglied();
					mitglied.setVorname(textFieldFirstName.getText());
					mitglied.setName(textFieldLastName.getText());
					mitglied.setPhone(textFieldPhone.getText());
					mitglied.setMail(textFieldMail.getText());
					mitglied.setMaxArtikel(Integer.valueOf(textFieldMaxArticle.getText()));
					saveMember(mitglied.getName(), mitglied.getVorname(), mitglied.getPhone(), mitglied.getMail(), mitglied.getMaxArtikel());
					String name = mitglied.getName()+", "+mitglied.getVorname();
					
					comboBoxMemberChooserMember.addItem(name);
					comboBoxMemberChooserMember.updateUI();
					comboBoxMemberChooser.addItem(name);
					comboBoxMemberChooser.updateUI();
					textFieldFirstName.setText("");
					textFieldLastName.setText("");
					textFieldPhone.setText("");
					textFieldMail.setText("");
					textFieldMaxArticle.setText(String.valueOf(defaultMaxLeihartikel));
				} else {
					
					mitglied.setVorname(textFieldFirstName.getText());
					mitglied.setName(textFieldLastName.getText());
					mitglied.setPhone(textFieldPhone.getText());
					mitglied.setMail(textFieldMail.getText());
					mitglied.setMaxArtikel(Integer.valueOf(textFieldMaxArticle.getText()));
					try {
						db.updateMemberData("MITGLIEDER", mitglied.getName(), mitglied.getVorname(), mitglied.getPhone(), mitglied.getMail(), mitglied.getMaxArtikel());
						JOptionPane.showMessageDialog(null,"Mitgliedsdaten aktualisiert!",
						        "Matti", JOptionPane.OK_OPTION);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
		});
		btnMitgliedSpeichern.setForeground(Color.DARK_GRAY);
		btnMitgliedSpeichern.setBounds(540, 230, 170, 25);
		btnMitgliedSpeichern.setFont(UIManager.getFont("FormattedTextField.font"));
		panelMitgliedVerwalten.add(btnMitgliedSpeichern);
		
		//Erst wenn Nachname eingetragen wurde darf speichern aktiviert werden.
				
		textFieldLastName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				
				btnMitgliedSpeichern.setEnabled(true);
			}
		});
		
		
		
		
		/*Alle Componenten für Leihartikel verwalten*/
		final JPanel panelLeihartVerwalten = new JPanel();
		panelLeihartVerwalten.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentHidden(ComponentEvent e) {
				
				resetComponentPanelArticle();
			}
		});
		panelLeihartVerwalten.setBorder(new EtchedBorder(EtchedBorder.LOWERED, Color.WHITE, Color.LIGHT_GRAY));
		panelMain.add(panelLeihartVerwalten, "Leihartikel verwalten");
		panelLeihartVerwalten.setLayout(null);
		
		JLabel lblTitleArtikelNeu = new JLabel("Leihartikel");
		lblTitleArtikelNeu.setForeground(UIManager.getColor("List.dropLineColor"));
		lblTitleArtikelNeu.setBounds(22, 25, 140, 25);
		lblTitleArtikelNeu.setFont(UIManager.getFont("Button.font"));
		panelLeihartVerwalten.add(lblTitleArtikelNeu);
		newArticleFlag = false;
		
		final JComboBox comboBoxArticleChooser = new JComboBox();
		NewArticleMutableModel cbartChooserModel = new NewArticleMutableModel();
		comboBoxArticleChooser.setModel(cbartChooserModel);
		String[] artString = new String[db.getArticleArray("ARTIKEL",1).length];
		artString = db.getArticleArray("ARTIKEL",3);
		String[] artNrStr = new String[db.getArticleArray("ARTIKEL",1).length];
		artNrStr = db.getArticleArray("ARTIKEL",1);
		
		for (int i = 0; i < artString.length; i++) {	
			comboBoxArticleChooser.addItem(new ComboBoxNewArticle(artString[i],Integer.parseInt(artNrStr[i])));
		}
		comboBoxArticleChooser.setForeground(Color.DARK_GRAY);
		comboBoxArticleChooser.setSelectedIndex(-1);
		comboBoxArticleChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int artNum = extractArtNum((String)comboBoxArticleChooser.getSelectedItem(), "-");
				String[] artStr = db.readArticleRow("ARTIKEL", artNum);
				textFieldBezeichnung.setText(artStr[1]);
				textFieldArtikelNr.setText(String.valueOf(artNum));
				textFieldArtikelNr.setEditable(false);
				textFieldBestand.setText(artStr[3]);
				textFieldBestand.setEditable(true);
				textFieldBestandAvailable.setText(artStr[2]);
				
				btnArtikelOK.setEnabled(true);
				}
			});
		comboBoxArticleChooser.setFont(UIManager.getFont("FormattedTextField.font"));
		comboBoxArticleChooser.setBounds(550, 70, 150, 25);
		panelLeihartVerwalten.add(comboBoxArticleChooser);
		
		JLabel lblBezeichnung = new JLabel("Artikelbezeichnung");
		lblBezeichnung.setForeground(Color.DARK_GRAY);
		lblBezeichnung.setBounds(22, 70, 160, 15);
		lblBezeichnung.setFont(UIManager.getFont("FormattedTextField.font"));
		panelLeihartVerwalten.add(lblBezeichnung);
		
		textFieldBezeichnung = new JTextField();
		textFieldBezeichnung.setForeground(Color.DARK_GRAY);
		textFieldBezeichnung.setBounds(180, 70, 300, 20);
		textFieldBezeichnung.setFont(UIManager.getFont("FormattedTextField.font"));
		panelLeihartVerwalten.add(textFieldBezeichnung);
		textFieldBezeichnung.setColumns(2);
		
		JLabel lblArtikelNr = new JLabel("Art.-Nummer");
		lblArtikelNr.setForeground(Color.DARK_GRAY);
		lblArtikelNr.setBounds(22, 100, 160, 15);
		lblArtikelNr.setFont(UIManager.getFont("FormattedTextField.font"));
		panelLeihartVerwalten.add(lblArtikelNr);
		
		textFieldArtikelNr = new JTextField();
		textFieldArtikelNr.setForeground(Color.DARK_GRAY);
		textFieldArtikelNr.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldArtikelNr.setBounds(180, 100, 300, 20);
		panelLeihartVerwalten.add(textFieldArtikelNr);
		textFieldArtikelNr.setColumns(10);
		
		JLabel lblBestand = new JLabel("Bestand");
		lblBestand.setForeground(Color.DARK_GRAY);
		lblBestand.setFont(UIManager.getFont("FormattedTextField.font"));
		lblBestand.setBounds(22, 130, 160, 15);
		panelLeihartVerwalten.add(lblBestand);
		
		textFieldBestand = new JTextField();
		textFieldBestand.setForeground(Color.DARK_GRAY);
		textFieldBestand.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldBestand.setBounds(180, 130, 300, 20);
		panelLeihartVerwalten.add(textFieldBestand);
		textFieldBestand.setColumns(10);
		
		JLabel lblBestandAvailable = new JLabel("Bestand verfügbar");
		lblBestandAvailable.setForeground(Color.DARK_GRAY);
		lblBestandAvailable.setFont(UIManager.getFont("FormattedTextField.font"));
		lblBestandAvailable.setBounds(22, 160, 160, 15);
		panelLeihartVerwalten.add(lblBestandAvailable);
		
		textFieldBestandAvailable = new JTextField();
		textFieldBestandAvailable.setForeground(Color.DARK_GRAY);
		textFieldBestandAvailable.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldBestandAvailable.setBounds(180, 160, 300, 20);
		textFieldBestandAvailable.setEditable(false);
		panelLeihartVerwalten.add(textFieldBestandAvailable);
		textFieldBestandAvailable.setColumns(10);
	
		btnArticelNew = new JButton("Neu");
		btnArticelNew.setForeground(Color.DARK_GRAY);
		btnArticelNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
						
				textFieldBezeichnung.setText("");
				textFieldBezeichnung.setEditable(true);
				int lastArtNum = db.getArticleTables("ARTIKEL")+1;
				textFieldArtikelNr.setText(String.valueOf(lastArtNum));
				textFieldArtikelNr.setEditable(true);
				textFieldBestand.setText("");
				textFieldBestand.setEditable(true);
				newArticleFlag = true;
				
				}
			});
		btnArticelNew.setFont(UIManager.getFont("FormattedTextField.font"));
		btnArticelNew.setBounds(180, 200, 100, 25);
		panelLeihartVerwalten.add(btnArticelNew);
		
		btnArtikelOK = new JButton("Speichern");
		//btnArtikelOK.setBackground(UIManager.getColor("Button.select"));
		btnArtikelOK.setForeground(Color.DARK_GRAY);
		btnArtikelOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
						
				saveArtikel(textFieldBezeichnung.getText(), Integer.parseInt(textFieldArtikelNr.getText()), Integer.parseInt(textFieldBestand.getText()), newArticleFlag);
				comboBoxArticleChooser.addItem(new ComboBoxNewArticle(textFieldBezeichnung.getText(),Integer.parseInt(textFieldArtikelNr.getText())));
				comboBoxArticleChooser.updateUI();
				}
			});
		btnArtikelOK.setFont(UIManager.getFont("FormattedTextField.font"));
		btnArtikelOK.setBounds(300, 200, 100, 25);
		panelLeihartVerwalten.add(btnArtikelOK);
		
		//Erst wenn Artikelbezeichnung eingetragen wurde darf speichern aktiviert werden.
		
				textFieldBezeichnung.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						
						btnArtikelOK.setEnabled(true);
					}
				});

		//alle Komponenten für Panel Preferences
		final JPanel panelPrefs = new JPanel();
		panelPrefs.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent e) {
				restorePrefs();
			}
		});
		panelPrefs.setBorder(new EtchedBorder(EtchedBorder.LOWERED, Color.WHITE, Color.LIGHT_GRAY));
		panelMain.add(panelPrefs, "Voreinstellungen");
		panelPrefs.setLayout(null);
		panelPrefs.repaint();
		
		JLabel lblHost = new JLabel("Host");
		lblHost.setForeground(Color.DARK_GRAY);
		lblHost.setBounds(22, 100, 160, 15);
		lblHost.setFont(UIManager.getFont("FormattedTextField.font"));
		panelPrefs.add(lblHost);
		
		textFieldHost = new JTextField();
		textFieldHost.setForeground(Color.DARK_GRAY);
		textFieldHost.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldHost.setBounds(180, 100, 300, 20);
		panelPrefs.add(textFieldHost);
		textFieldHost.setColumns(10);
		
		textFieldMailFrom = new JTextField();
		textFieldMailFrom.setForeground(Color.DARK_GRAY);
		textFieldMailFrom.setBounds(180, 70, 300, 20);
		textFieldMailFrom.setFont(UIManager.getFont("FormattedTextField.font"));
		panelPrefs.add(textFieldMailFrom);
		textFieldMailFrom.setColumns(2);
		
		JLabel lblFrom = new JLabel("Absender Mailadresse");
		lblFrom.setForeground(Color.DARK_GRAY);
		lblFrom.setBounds(22, 70, 160, 15);
		lblFrom.setFont(UIManager.getFont("FormattedTextField.font"));
		panelPrefs.add(lblFrom);
		
		JLabel lblTitleLabel = new JLabel("Maileinstellungen");
		lblTitleLabel.setForeground(UIManager.getColor("List.dropLineColor"));
		lblTitleLabel.setBounds(22, 25, 140, 25);
		lblTitleLabel.setFont(UIManager.getFont("Button.font"));
		panelPrefs.add(lblTitleLabel);
		
		textFieldPort = new JTextField();
		textFieldPort.setForeground(Color.DARK_GRAY);
		textFieldPort.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldPort.setBounds(180, 130, 300, 20);
		panelPrefs.add(textFieldPort);
		textFieldPort.setColumns(10);
		
		JLabel lblUser = new JLabel("User");
		lblUser.setForeground(Color.DARK_GRAY);
		lblUser.setFont(UIManager.getFont("FormattedTextField.font"));
		lblUser.setBounds(22, 160, 160, 15);
		panelPrefs.add(lblUser);
		
		JLabel lblPort = new JLabel("Port");
		lblPort.setForeground(Color.DARK_GRAY);
		lblPort.setFont(UIManager.getFont("FormattedTextField.font"));
		lblPort.setBounds(22, 130, 160, 15);
		panelPrefs.add(lblPort);
		
		textFieldUser = new JTextField();
		textFieldUser.setForeground(Color.DARK_GRAY);
		textFieldUser.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldUser.setBounds(180, 160, 300, 20);
		panelPrefs.add(textFieldUser);
		textFieldUser.setColumns(10);
		
		JLabel lblPasswort = new JLabel("Passwort");
		lblPasswort.setForeground(Color.DARK_GRAY);
		lblPasswort.setFont(UIManager.getFont("FormattedTextField.font"));
		lblPasswort.setBounds(22, 190, 160, 15);
		panelPrefs.add(lblPasswort);
		
		textFieldPassword = new JTextField();
		textFieldPassword.setForeground(Color.DARK_GRAY);
		textFieldPassword.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldPassword.setBounds(180, 190, 300, 20);
		panelPrefs.add(textFieldPassword);
		textFieldPassword.setColumns(10);
		
		JLabel lblStarttls = new JLabel("StartTLS");
		lblStarttls.setForeground(Color.DARK_GRAY);
		lblStarttls.setFont(UIManager.getFont("FormattedTextField.font"));
		lblStarttls.setBounds(22, 220, 160, 15);
		panelPrefs.add(lblStarttls);
		
		ckbxEnable = new JCheckBox("Enable");
		ckbxEnable.setForeground(Color.DARK_GRAY);
		ckbxEnable.setFont(UIManager.getFont("FormattedTextField.font"));
		ckbxEnable.setBounds(180, 220, 120, 20);
		panelPrefs.add(ckbxEnable);
		
		JLabel lblPfadeinstellungenDatenbank = new JLabel("Pfadeinstellungen Datenbank");
		lblPfadeinstellungenDatenbank.setForeground(UIManager.getColor("List.dropLineColor"));
		lblPfadeinstellungenDatenbank.setFont(UIManager.getFont("Button.font"));
		lblPfadeinstellungenDatenbank.setBounds(22, 295, 220, 15);
		panelPrefs.add(lblPfadeinstellungenDatenbank);
		
		JLabel lblDbPfad = new JLabel("DB Pfad");
		lblDbPfad.setForeground(Color.DARK_GRAY);
		lblDbPfad.setFont(UIManager.getFont("FormattedTextField.font"));
		lblDbPfad.setBounds(22, 340, 70, 15);
		panelPrefs.add(lblDbPfad);
		
		textFieldDbPath = new JTextField();
		textFieldDbPath.setForeground(Color.DARK_GRAY);
		textFieldDbPath.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldDbPath.setBounds(180, 340, 300, 20);
		panelPrefs.add(textFieldDbPath);
		textFieldDbPath.setColumns(10);
		
		JButton btnFj = new JButton("");
		btnFj.setIcon(new ImageIcon(MattiGUI.class.getResource("/icons32/folder32.png")));
		btnFj.setToolTipText("Ordner auswählen");
		btnFj.setForeground(Color.DARK_GRAY);
		btnFj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//kein rename der Ordner
				UIManager.put("FileChooser.readOnly", Boolean.TRUE);
				JFileChooser dbChooser = new JFileChooser(".");
				dbChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				dbChooser.setFileHidingEnabled(true);
		        int returnVal = dbChooser.showOpenDialog(null);
		        File f;
		        if (returnVal == JFileChooser.APPROVE_OPTION)
		        {
		            f = dbChooser.getSelectedFile();
		            textFieldDbPath.setText(dbChooser.getSelectedFile().toString());
		        }
			}
		});
		btnFj.setBounds(500, 335, 50, 30);
		panelPrefs.add(btnFj);
		
		JLabel lblVoreinstellungenLeihe = new JLabel("Voreinstellungen Leihe");
		lblVoreinstellungenLeihe.setForeground(UIManager.getColor("List.dropLineColor"));
		lblVoreinstellungenLeihe.setFont(UIManager.getFont("Button.font"));
		lblVoreinstellungenLeihe.setBounds(22, 385, 170, 15);
		panelPrefs.add(lblVoreinstellungenLeihe);
		
		JLabel lblLeihart1 = new JLabel("Leihart 1");
		lblLeihart1.setForeground(Color.DARK_GRAY);
		lblLeihart1.setFont(UIManager.getFont("FormattedTextField.font"));
		lblLeihart1.setBounds(22, 430, 160, 15);
		panelPrefs.add(lblLeihart1);
		
		textFieldLeihart1 = new JTextField();
		textFieldLeihart1.setForeground(Color.DARK_GRAY);
		textFieldLeihart1.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldLeihart1.setBounds(180, 430, 300, 20);
		panelPrefs.add(textFieldLeihart1);
		textFieldLeihart1.setColumns(10);
		
		JLabel lblLeihart2 = new JLabel("Leihart 2");
		lblLeihart2.setForeground(Color.DARK_GRAY);
		lblLeihart2.setFont(UIManager.getFont("FormattedTextField.font"));
		lblLeihart2.setBounds(22, 460, 160, 15);
		panelPrefs.add(lblLeihart2);
		
		textFieldLeihart2 = new JTextField();
		textFieldLeihart2.setForeground(Color.DARK_GRAY);
		textFieldLeihart2.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldLeihart2.setBounds(180, 460, 300, 20);
		panelPrefs.add(textFieldLeihart2);
		textFieldLeihart2.setColumns(10);
		
		JLabel lblLeihart3 = new JLabel("Leihart 3");
		lblLeihart3.setForeground(Color.DARK_GRAY);
		lblLeihart3.setFont(UIManager.getFont("FormattedTextField.font"));
		lblLeihart3.setBounds(22, 490, 160, 15);
		panelPrefs.add(lblLeihart3);
		
		textFieldLeihart3 = new JTextField();
		textFieldLeihart3.setForeground(Color.DARK_GRAY);
		textFieldLeihart3.setFont(UIManager.getFont("FormattedTextField.font"));
		textFieldLeihart3.setBounds(180, 490, 300, 20);
		panelPrefs.add(textFieldLeihart3);
		textFieldLeihart3.setColumns(10);
		
		JLabel lblDefaultMaxleihartikel = new JLabel("Default max. Leihart.");
		lblDefaultMaxleihartikel.setForeground(Color.DARK_GRAY);
		lblDefaultMaxleihartikel.setFont(UIManager.getFont("FormattedTextField.font"));
		lblDefaultMaxleihartikel.setBounds(22, 520, 160, 15);
		panelPrefs.add(lblDefaultMaxleihartikel);
		
		textFieldDefaultMaxLeihartikel = new JTextField();
		textFieldDefaultMaxLeihartikel.setForeground(Color.DARK_GRAY);
		textFieldDefaultMaxLeihartikel.setBounds(180, 520, 300, 20);
		panelPrefs.add(textFieldDefaultMaxLeihartikel);
		textFieldDefaultMaxLeihartikel.setColumns(10);
		
		JButton btnEditMail1 = new JButton("");
		btnEditMail1.setToolTipText("Message Text und Subject für Leihe zurück");
		btnEditMail1.setIcon(new ImageIcon(MattiGUI.class.getResource("/icons48/messagetext-mailin48.png")));
		textFieldSubjectLn = new JTextField();
		textAreaMessageLn = new JTextArea();
		btnEditMail1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//Dialog erzeugen - Eingabe von subject und message
				textFieldSubjectLn.setText(emailSubjectLn);
				textAreaMessageLn.setText(emailMessageTextLn);
				textAreaMessageLn.setRows(5);
				textAreaMessageLn.setColumns(15);
				textAreaMessageLn.setEditable(true);
				JScrollPane messagePane = new JScrollPane(textAreaMessageLn);
				Object[] mailtext = {"Subject", textFieldSubjectLn, "Mailtext", messagePane,};
				JOptionPane mailJpane = new JOptionPane(mailtext, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION); 
				mailJpane.createDialog(null, "Matti").setVisible(true);
				
				String subjectStrLn = textFieldSubjectLn.getText();
				String messageStrLn = textAreaMessageLn.getText();
				
			}
		});
		btnEditMail1.setForeground(Color.DARK_GRAY);
		btnEditMail1.setFont(UIManager.getFont("FormattedTextField.font"));
		btnEditMail1.setBounds(280, 250, 70, 40);
		panelPrefs.add(btnEditMail1);
		
		JButton btnEditMail2 = new JButton("");
		btnEditMail2.setToolTipText("Message Text und Subject für Leihe neu");
		btnEditMail2.setIcon(new ImageIcon(MattiGUI.class.getResource("/icons48/messagetext-mailout48.png")));
		textFieldSubjectLz = new JTextField();
		textAreaMessageLz = new JTextArea();
		btnEditMail2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//Dialog erzeugen - Eingabe von subject und message
				textFieldSubjectLz.setText(emailSubjectLz);
				textAreaMessageLz.setText(emailMessageTextLz);
				textAreaMessageLz.setRows(5);
				textAreaMessageLz.setColumns(15);
				textAreaMessageLz.setEditable(true);
				JScrollPane messagePane = new JScrollPane(textAreaMessageLz);
				Object[] mailtext = {"Subject", textFieldSubjectLz, "Mailtext", messagePane,};
				JOptionPane mailJpane = new JOptionPane(mailtext, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION); 
				mailJpane.createDialog(null, "Matti").setVisible(true);
				
				String subjectStrLz = textFieldSubjectLz.getText();
				String messageStrLz = textAreaMessageLz.getText();
				
			}
		});
		btnEditMail2.setForeground(Color.DARK_GRAY);
		btnEditMail2.setFont(UIManager.getFont("FormattedTextField.font"));
		btnEditMail2.setBounds(180, 250, 70, 40);
		panelPrefs.add(btnEditMail2);
		
		JButton btnOk = new JButton("Speichern");
		btnOk.setForeground(Color.DARK_GRAY);
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				writePrefs.setRent_1(textFieldLeihart1.getText());
				mattiProperties[0] = writePrefs.getRent_1();
				writePrefs.setRent_2(textFieldLeihart2.getText());
				mattiProperties[1] = writePrefs.getRent_2();
				writePrefs.setRent_3(textFieldLeihart3.getText());
				mattiProperties[2] = writePrefs.getRent_3();
				writePrefs.setDefaultMaxArtikel(Integer.parseInt(textFieldDefaultMaxLeihartikel.getText()));
				mattiProperties[3] = String.valueOf(writePrefs.getDefaultMaxArtikel());
				writePrefs.setEmail_from(textFieldMailFrom.getText());
				mattiProperties[4] = writePrefs.getEmail_from();
				writePrefs.setEmail_host(textFieldHost.getText());
				mattiProperties[5] = writePrefs.getEmail_host();
				writePrefs.setEmail_port(textFieldPort.getText());
				mattiProperties[6] = writePrefs.getEmail_port();
				writePrefs.setEmail_user(textFieldUser.getText());
				mattiProperties[7] = writePrefs.getEmail_user();
				writePrefs.setEmail_password(textFieldPassword.getText());
				mattiProperties[8] = writePrefs.getEmail_password();
				writePrefs.setEmail_startTls(ckbxEnable.isSelected());
				mattiProperties[9] = String.valueOf(writePrefs.getEmail_startTls());
				writePrefs.setEmail_smtpAuth(true);
				mattiProperties[10] = String.valueOf(writePrefs.getEmail_smtpAuth());
				writePrefs.setPath_To_DB(textFieldDbPath.getText());
				mattiProperties[11] = writePrefs.getPath_To_DB();
				mattiProperties[12] = String.valueOf(writePrefs.getFirst_Run());
				writePrefs.setEmail_subjectLn(textFieldSubjectLn.getText());
				mattiProperties[13] = writePrefs.getEmail_subjectLn();
				writePrefs.setEmail_messageTextLn(textAreaMessageLn.getText());
				mattiProperties[14] = writePrefs.getEmail_messageTextLn();
				writePrefs.setEmail_subjectLz(textFieldSubjectLz.getText());
				mattiProperties[15] = writePrefs.getEmail_subjectLz();
				writePrefs.setEmail_messageTextLz(textAreaMessageLz.getText());
				mattiProperties[16] = writePrefs.getEmail_messageTextLz();
						
				//Schreibe Voreinstellungen und lade sie wieder
				writePrefs.saveParamChangesAsXML(mattiProperties);
				loadParams();
				JOptionPane.showMessageDialog(null, "Einstellungen wurden gespeichert.",
				        "Matti", JOptionPane.OK_OPTION);
			}
		});
		btnOk.setFont(UIManager.getFont("FormattedTextField.font"));
		btnOk.setBounds(240, 600, 115, 25);
		panelPrefs.add(btnOk);
		
		JButton btnRestore = new JButton("Restore");
		btnRestore.setForeground(Color.DARK_GRAY);
		btnRestore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				restorePrefs();
			}
		});
		btnRestore.setFont(UIManager.getFont("FormattedTextField.font"));
		btnRestore.setBounds(365, 600, 115, 25);
		panelPrefs.add(btnRestore);
		
		//alle Komponenten für die Hilfe
		JPanel panelHelp = new JPanel();
		panelHelp.setBorder(new EtchedBorder(EtchedBorder.LOWERED, Color.WHITE, Color.LIGHT_GRAY));
		panelMain.add(panelHelp, "Hilfe");
		panelHelp.setLayout(null);
		
		JTextPane textPaneHelp = new JTextPane();
		textPaneHelp.setBackground(UIManager.getColor("CheckBox.background"));
		textPaneHelp.setFont(UIManager.getFont("FormattedTextField.font"));
		
		JScrollPane scrollPane_help = new JScrollPane(textPaneHelp);
		scrollPane_help.setBounds(25, 7, 825, 550);
		scrollPane_help.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_help.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		panelHelp.add(scrollPane_help);
		scrollPane_help.setViewportView(textPaneHelp);
		// EditorKit erzeugen
		javax.swing.text.html.HTMLEditorKit eKit = new javax.swing.text.html.HTMLEditorKit();
		// Datei holen
		BufferedReader reader = new BufferedReader(new FileReader(new File("html/help.html")));
		String htmlString = "";
		int line;
		// Lese die HTML Datei aus
		try {
			while ((line = reader.read()) != -1) {
				htmlString += (char)line;
				}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		
		// EditorKit setzen
		textPaneHelp.setEditorKit(eKit);
		textPaneHelp.setFont(UIManager.getFont("FormattedTextField.font"));
		// Text setzen
		textPaneHelp.setText(htmlString);

	}
	
	public String showMember (int member) {
		ResultSet queryRS = null;
    	
    	//Ermitteln des selektierten Mitglieds
    	String[] selectedMitglied = db.returnTable(member+1,"MITGLIEDER");
    	mitglied.setName(selectedMitglied[0]);
    	mitglied.setVorname(selectedMitglied[1]);
    	String table = selectedMitglied[2];
    	int maxArt = Integer.parseInt(selectedMitglied[3]);
    	mitglied.setMaxArtikel(maxArt);
    	mitglied.setPhone(selectedMitglied[4]);
    	mitglied.setMail(selectedMitglied[5]);
    	
    	//Auslesen der zum Mitglied gehörenden TABLE
    	queryRS = db.readFromDatabase(table);
    	
    return table;
	}
	
	public ResultSet readMemberArticleData (String table) {
		
		ResultSet queryRS = null;
		queryRS = db.readFromDatabase(table);
		
				
		return queryRS;
	}
	
	public void saveMember (String lastName, String firstName, String phone, String mail, int maxArticle){
		
		//Testen ob Bindestriche im Vor- und Zunamen enthalten sind und entfernen für Tabellenname
		String tableLastName = StringUtils.remove(lastName, "-");
		String tableFirstName = StringUtils.remove(firstName, "-");
		
		//Tabellenname aus Vor-und Zunamen erstellen
    	String tableName = "MATTITABLE_"+tableLastName.toUpperCase()+"_"+tableFirstName.toUpperCase();
    	
    	//Schreiben der Daten in die Datenbank
    	try {
			db.writeMemberToDB("MITGLIEDER", lastName, firstName, phone, mail, maxArticle, tableName);
			JOptionPane.showMessageDialog(null, "Mitglied erfolgreich gespeichert.",
			        "Matti", JOptionPane.OK_OPTION);
			//System.out.println("Datensatz erfolgreich gespeichert.\n");
		} catch (SQLException e) {
			System.err.println("Fehler beim Schreiben in Datenbank!");
			e.printStackTrace();
		}
	}
	
	public void saveArtikel(String artikelBezeichnung, int artikelNumber, int artikelStock, Boolean isNewArticle) {
		
		Artikel artikel = new Artikel();
		int lastArtNum = db.getArticleTables("ARTIKEL");
		
		artikel.setArtikel(artikelBezeichnung);
		lastArtNum = lastArtNum + 1;
    	if (lastArtNum == 0) {
			lastArtNum = 100;
		} 
    	if (artikelNumber == 0) {
    		artikel.setArtikelNummer(lastArtNum);
		} else {
			
			artikel.setArtikelNummer(artikelNumber);
		}
    	
    	artikel.setArtikelStock(artikelStock);
    	artikel.setStockAvailable(artikel.getArtikelStock());
		
    	if (isNewArticle == true) {
			artikel.setIsNewArticle(true);
		} else {
			artikel.setIsNewArticle(false);
		}
    	
    	//Schreiben der Artikeldaten in DB
    	if (artikel.getIsNewArticle() == true) {
    		try {
    			db.writeToArticleTable("ARTIKEL", artikel.getArtikel(), artikel.getArtikelStock(), artikel.getArtikelNummer(), artikel.getStockAvailable());
    			JOptionPane.showMessageDialog(null, "Artikel erfolgreich gespeichert.",
				        "Matti", JOptionPane.OK_OPTION);
    		} catch (SQLException e) {
    			System.err.println("Fehler saveArtikel/getIsNewArticle=true");
    			e.printStackTrace();
    		}
		} else {
			try {
				db.updateArticleTable("ARTIKEL", artikel.getArtikel(), artikel.getArtikelStock(), artikel.getArtikelNummer());
				JOptionPane.showMessageDialog(null, "Artikelupdate erfolgreich gespeichert.",
				        "Matti", JOptionPane.OK_OPTION);
			} catch (Exception e) {
				System.err.println("Fehler saveArtikel/getIsNewArticle=false");
			}
		}
	}
	
	public int extractArtNum (String artikelStr, String separator){
		
		String [] extractStr = new String [2];
		extractStr = StringUtils.split(artikelStr, separator);
		
		return Integer.parseInt(extractStr[0].trim());
		
	}
	
	public static void loadParams() {
	    Properties props = new Properties();
	    InputStream is = null;
	 
	    // Lade Datei
	    try {
	        File f = new File("matti.ini");
	        is = new FileInputStream( f );
	    }
	    catch ( Exception e ) { is = null; 
	   
	    System.out.println("Datei matti.ini nicht vorhanden...verwende Default-Werte.");
	    }
	    try {
	      
	        props.loadFromXML(is);
	    }
	    catch ( Exception e ) { }
	 
	    rental1 = props.getProperty("RENT_1", "*");
	    rental2 = props.getProperty("RENT_2", "*");
	    rental3 = props.getProperty("RENT_3", "*");
	    defaultMaxLeihartikel = Integer.parseInt(props.getProperty("DEFAULT_MAX_ARTIKEL", "3"));
	    
	    emailFrom = props.getProperty("EMAIL_FROM", "*");
	    emailHost = props.getProperty("EMAIL_HOST", "*");
	    emailPort = props.getProperty("EMAIL_PORT", "587");
	    emailUser = props.getProperty("EMAIL_USER", "*");
	    emailPassword = props.getProperty("EMAIL_PASSWD", "*");
	    emailStartTLS = Boolean.parseBoolean(props.getProperty("EMAIL_STARTTLS", "true"));
	    emailSmtpAuth = Boolean.parseBoolean(props.getProperty("EMAIL_SMTPAUTH", "true"));
	    emailSubjectLn = props.getProperty("EMAIL_SUBJECTLN", "This is a message generated by Matti");
	    emailMessageTextLn = props.getProperty("EMAIL_MESSAGETEXTLN", "This is an empty message created by Matti");
	    emailSubjectLz = props.getProperty("EMAIL_SUBJECTLZ", "This is a message generated by Matti");
	    emailMessageTextLz = props.getProperty("EMAIL_MESSAGETEXTLZ", "This is an empty message created by Matti");
	    
	    String defaultPath = "jdbc:h2:"+System.getProperty("user.dir")+System.getProperty("file.separator")+"Database"+System.getProperty("file.separator")+"matti_db";
	    
	    dbPath = props.getProperty("PATH_TO_DB", defaultPath);
	    
	    firstRun = Boolean.parseBoolean(props.getProperty("FIRST_RUN", "true"));
	}
	
	public static void FirstRun() {
		
		writePrefs.setEmail_from(emailFrom);
		writePrefs.setEmail_host(emailHost);
		writePrefs.setEmail_port(emailPort);
		writePrefs.setEmail_user(emailUser);
		writePrefs.setEmail_password(emailPassword);
		writePrefs.setEmail_startTls(emailStartTLS);
		writePrefs.setEmail_smtpAuth(emailSmtpAuth);
		writePrefs.setEmail_subjectLn(emailSubjectLn);
		writePrefs.setEmail_messageTextLn(emailMessageTextLn);
		writePrefs.setEmail_subjectLz(emailSubjectLz);
		writePrefs.setEmail_messageTextLz(emailMessageTextLz);
		writePrefs.setPath_To_DB(dbPath);
		writePrefs.setRent_1(rental1);
		writePrefs.setRent_2(rental2);
		writePrefs.setRent_3(rental3);
		writePrefs.setDefaultMaxArtikel(defaultMaxLeihartikel);
		writePrefs.setFirst_Run(false);
		
		mattiProperties[0] = writePrefs.getRent_1();
		mattiProperties[1] = writePrefs.getRent_2();
		mattiProperties[2] = writePrefs.getRent_3();
		mattiProperties[3] = String.valueOf(writePrefs.getDefaultMaxArtikel());
		mattiProperties[4] = writePrefs.getEmail_from();
		mattiProperties[5] = writePrefs.getEmail_host();
		mattiProperties[6] = writePrefs.getEmail_port();
		mattiProperties[7] = writePrefs.getEmail_user();
		mattiProperties[8] = writePrefs.getEmail_password();
		mattiProperties[9] = String.valueOf(writePrefs.getEmail_startTls());
		mattiProperties[10] = String.valueOf(writePrefs.getEmail_smtpAuth());		
		mattiProperties[11] = writePrefs.getPath_To_DB();
		mattiProperties[12] = String.valueOf(writePrefs.getFirst_Run());
		mattiProperties[13] = writePrefs.getEmail_subjectLn();
		mattiProperties[14] = writePrefs.getEmail_messageTextLn();
		mattiProperties[15] = writePrefs.getEmail_subjectLz();
		mattiProperties[16] = writePrefs.getEmail_messageTextLz();
		
		writePrefs.saveParamChangesAsXML(mattiProperties);
		
		db.createAllTables("MITGLIEDER","ARTIKEL");
	}
	
	class JFormattedDateTextField extends JFormattedTextField {
		 
		private static final long serialVersionUID = 1L;
		Format format = new SimpleDateFormat("dd-MM-yyyy");
		  
		   public JFormattedDateTextField() {
		      super();
		      MaskFormatter maskFormatter = null;
		      try {
		         maskFormatter = new MaskFormatter("##-##-####");
		      } catch (ParseException e) {
		         e.printStackTrace();
		      }
		  
		      maskFormatter.setPlaceholderCharacter('_');
		      setFormatterFactory(new DefaultFormatterFactory(maskFormatter));
		      this.addFocusListener(new FocusAdapter() {
		         public void focusGained(FocusEvent e) {
		            if (getFocusLostBehavior() == JFormattedTextField.PERSIST)
		               setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
		            }
		   
		            public void focusLost(FocusEvent e) {
		               try {
		                  Date date = (Date) format.parseObject(getText());
		                  setValue(format.format(date));
		               } catch (ParseException pe) {
		                  setFocusLostBehavior(JFormattedTextField.PERSIST);
		                  setText("");
		                  setValue(null);
		               }
		            }
		      });
		   }
		  
		   public void setValue(Date date) {
		      super.setValue(toString(date));
		   }
		  
		   private String toString(Date date) {
		      try {
		         return format.format(date);
		      } catch (Exception e) {
		         return "";
		      }
		   }
		}

	
		public static TableModel emptyModel(){
			
			Vector columnNames = new Vector();
			// Get all rows.
            Vector rows = new Vector();
			// Set the column names
            columnNames.addElement("ID");
            columnNames.addElement("Artikel");
            columnNames.addElement("Leihart");
            columnNames.addElement("Ausgabedatum");
            columnNames.addElement("Status");
            columnNames.addElement("Rückgabedatum");
            
            Vector newRow = new Vector();
        	for (int i = 1; i <= 6; i++) {
        		newRow.addElement("no data");
        	
        	}
        	rows.addElement(newRow);
            
            return new DefaultTableModel(rows, columnNames);
		}
	    @SuppressWarnings({ "rawtypes", "unchecked" })
		public static TableModel resultSet2TableModel(ResultSet rs) {
	        try {
	            ResultSetMetaData metaData = rs.getMetaData();
	            int numberOfColumns = metaData.getColumnCount();
	            Vector columnNames = new Vector();

	            // Set the column names
	            columnNames.addElement("ID");
	            columnNames.addElement("Artikel");
	            columnNames.addElement("Leihart");
	            columnNames.addElement("Ausgabedatum");
	            columnNames.addElement("Status");
	            columnNames.addElement("Rückgabedatum");
	            
	            
	            // Get all rows.
	            Vector rows = new Vector();
	            
	            		while (rs.next()) {
	            			Vector newRow = new Vector();
	            			for (int i = 1; i <= numberOfColumns; i++) {
	            				switch (i) {
	            				case 1:
	            					newRow.addElement(rs.getObject(4));
	            					break;
	            				case 2:
	            					String[] articleFromNumber = new String[3]; 
	            					articleFromNumber = db.readArticleRow("ARTIKEL", rs.getInt(1));
	            					newRow.addElement(articleFromNumber[1]);
	            					break;
	            				case 3:
	            					newRow.addElement(rs.getObject(2));
	            					break;
	            				case 4:
	            					newRow.addElement(rs.getObject(3));
	            					break;
	            				case 5:
	            					if (rs.getBoolean(5)==true) {
	            						newRow.addElement("erledigt");
	                    		
	                    		
	            					} else {
	            						newRow.addElement("offen");
	            					}
	            					break;
	            				default:
	            					newRow.addElement(rs.getObject(i));
	            					break;
	            				} 
	            			}
	            			rows.addElement(newRow);
	            		}
	            
	            return new DefaultTableModel(rows, columnNames);
	        } catch (Exception e) {
	            e.printStackTrace();

	            return null;
	        }
	    }
	    
	    public ArrayList getIndex(String[] compareArray){
	    	//Erzeugt die Referenz zwischen dem sortierten und dem unsortierten Array
	    	
	    	String[] unsortedArray = db.getTables("MITGLIEDER",1);
	    	ArrayList<Integer> indexList = new ArrayList<Integer>();
	    	String key;
	    	
	    	Arrays.sort(compareArray);
	    	
	    	
	    	for (int i = 0; i < unsortedArray.length; i++) {
	    		key = unsortedArray[i];
	    		indexList.add(i, Arrays.binarySearch(compareArray, key));
			}
	    	
	    	return indexList;
	    }
	    
	    public String searchAndReplace(String searchString, int flag){
	    //sucht und ersetzt alle Platzhalter in den vorkonfigurierten Mailtexten	    	
	
	    	String returnString = "";
	    	Helper convert = new Helper(); 
	    	
	    	//Ersetze alle Variablen
	    	switch (flag) {
			case 0:
				String[] searchArrayLn = {"%name", "%vorname", "%artikel", "%leihart", "%date_out"};
		    	String[] replaceArrayLn = {mitglied.getName(), mitglied.getVorname(), mitglied.getLeihartikel(), mitglied.getLeihArt(), convert.convertDateToString(mitglied.getLeihDate())};
				returnString = StringUtils.replaceEach(searchString, searchArrayLn, replaceArrayLn);
				break;
				
			case 1:
				String[] searchArrayLz = {"%name", "%vorname", "%artikel", "%leihart", "%date_out", "%date_return"};
		    	String[] replaceArrayLz = {mitglied.getName(), mitglied.getVorname(), mitglied.getLeihartikel(), mitglied.getLeihArt(), convert.convertDateToString(mitglied.getAusgabeDate()), convert.convertDateToString(mitglied.getLeihDate())};
				returnString = StringUtils.replaceEach(searchString, searchArrayLz, replaceArrayLz);
				break;

			default:
				break;
			}
	    	
	    	return returnString;
	    }
	    
	    public void resetComponentPanelMitglied(){
	    //Resets the component to the initial states	
	    	
	    		textFieldFirstName.setEditable(false);
	    		textFieldFirstName.setText("");
				textFieldLastName.setEditable(false);
				textFieldLastName.setText("");
				textFieldPhone.setEditable(false);
				textFieldPhone.setText("");
				textFieldMail.setEditable(false);
				textFieldMail.setText("");
				textFieldMaxArticle.setEditable(false);
				textFieldMaxArticle.setText(String.valueOf(defaultMaxLeihartikel));
				
				btnMitgliedLschen.setEnabled(false);
				btnMitgliedSpeichern.setEnabled(false);
	
	    }
	    
	    public void resetComponentPanelAbfrage(){
	    	//Resets the component to the initial states
	    	btnLeiheNeu.setEnabled(false);
	    	btnLeiheZurck.setEnabled(false);
	    	
	    }
	    
	    public void resetComponentPanelArticle() {
	    //Resets the Component to the initial states
	    	
	    	textFieldBezeichnung.setEditable(false);
	    	textFieldBezeichnung.setText("");
	    	textFieldArtikelNr.setEditable(false);
	    	textFieldArtikelNr.setText("");
	    	textFieldBestand.setEditable(false);
	    	textFieldBestand.setText("");
	    	textFieldBestandAvailable.setText("");
	    	
	    	btnArtikelOK.setEnabled(false);
	    	
	    }
	    
	    public void restorePrefs() {
	    //Resets the Component to the stored values
	    	
	    	textFieldMailFrom.setText(writePrefs.getEmail_from());
			textFieldHost.setText(writePrefs.getEmail_host());
			textFieldPort.setText(writePrefs.getEmail_port());
			textFieldUser.setText(writePrefs.getEmail_user());
			textFieldPassword.setText(writePrefs.getEmail_password());
			ckbxEnable.setSelected(writePrefs.getEmail_startTls());
			textFieldDbPath.setText(writePrefs.getPath_To_DB());
			textFieldLeihart1.setText(writePrefs.getRent_1());
			textFieldLeihart2.setText(writePrefs.getRent_2());
			textFieldLeihart3.setText(writePrefs.getRent_3());
			textFieldDefaultMaxLeihartikel.setText(String.valueOf(writePrefs.getDefaultMaxArtikel()));
			textFieldSubjectLn.setText(writePrefs.getEmail_subjectLn());
			textAreaMessageLn.setText(writePrefs.getEmail_messageTextLn());
			textFieldSubjectLz.setText(writePrefs.getEmail_subjectLz());
			textAreaMessageLz.setText(writePrefs.getEmail_messageTextLz());
	    }
}
