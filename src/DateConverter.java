/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateConverter {
	
	public String ToString (Date date){
		
		Calendar datum = Calendar.getInstance();
		String strMonth;
		
		datum.setTime(date);
		int day = datum.get(Calendar.DATE);
		int month = datum.get(Calendar.MONTH)+1;
		int year = datum.get(Calendar.YEAR);
	
		//Workaround fuer 2-stellige Monatsanzeige
		//TODO geht auch einfacher
		if (month < 10) {
			strMonth = "0"+Integer.toString(month);
		}
		else {
			strMonth = Integer.toString(month);
		}
		String dateString = day+"-"+strMonth+"-"+year;
		
		return dateString;
		
	}
	
	public Date toDate (String dateString){
		Date date = null;
		SimpleDateFormat datum = new SimpleDateFormat("dd-MM-yyyy");
		
	      if (dateString == null) return null;
	      try {
	         
	    	 date = datum.parse(dateString);
	      }
	      catch (ParseException pe) {
	         // ignore
	      }
	  
	      return date;
	}

}
