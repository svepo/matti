/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

public class ComboBoxMember {
	
	private String lastFirstName;
	
	
	public ComboBoxMember(String lastFirstName){
        this.lastFirstName = lastFirstName;
        
    }
 
    public String getComboBoxMember(){
        return this.lastFirstName;
    }
 
    public String getLastFirstName(){
    	return this.lastFirstName;
    }
 
   
   

}
