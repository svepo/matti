/* 
 * Matti Freie Materialverwaltungssoftware für Vereine - http://matti-software.de
 * 
 * Copyright (C) 2015 Sven Powalla
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sven Powalla - initial program and implementation
 */

import java.util.Arrays;
import java.util.Iterator;

import java.util.Vector;

import javax.swing.DefaultComboBoxModel;

	public class SortedComboBoxModel extends DefaultComboBoxModel {
		
		
		
		public SortedComboBoxModel() {
			super();
		}
		/** 
		 * ersetzt den entsprechenden Konstruktor der Superklasse. Das 
		 * übergebene Array wird erst sortiert und anschließend mit der
		 * addItem-Methode der Superklasse eingebaut. Ich vertraue dabei 
		 * darauf, dass der Arrays.sort-Algorhythmus schneller ist als 
		 * die selbstgestrickte addItem-Methode dieser Klasse. 
		 * @param itemArray
		 */
		public SortedComboBoxModel(final Object[] itemArray){
			super();
			
			Arrays.sort(itemArray);
			for (int i = 0; i < itemArray.length; i++){
				super.addElement(itemArray[i]);
			}
		
		}
	        /**
	        * Ersetzt den Konstruktor der Superklasse. Hier wird für die Sortierung
	        * auf den eigenen Einfügemechanismus vertraut.
	        */
		public SortedComboBoxModel(Vector itemVector){
			super();
			Iterator it = itemVector.iterator();
			while (it.hasNext()){
				this.addElement(it.next());
			}
		}
		/**
		 * Die addElement-Methode durchläuft die vorhandenen Elemente und 
		 * sucht nach der geeigneten Position zum Einfügen.
		 */
		public void addElement(Object o){
			
			int min = 0;
			int max = this.getSize() - 1;
			int insertAt = -1;
			int compare = -2;
			while (min <= max){
				
				Object inList = this.getElementAt(min);
				compare = ((Comparable)inList).compareTo(o);
				if (compare > 0){
					insertAt = min;
					break;
				}
				min++;
			}
			if (insertAt == -1)
				super.addElement(o);
			else
				super.insertElementAt(o, insertAt);
			
		}

}
